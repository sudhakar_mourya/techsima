@extends('layout.home')
@section('frontdata')
<!-- banner section start -->
<section class="bg-white">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 background-slider">
                <div class="row background-slider-outer">
                    <div class="techsima-content">
                        <h1 class="techsima-title">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h1>
                        <p class="techsima-des">Lorem ipsum- dolor sit amet consectetur, adipisicing elit. Autem ut
                            laudantium natus est voluptatem ex dolore quam! Voluptate inventore, repellendus veritatis
                            doloremque repudiandae alias eaque molestiae illum, architecto maxime aspernatur.</p>
                        <a class="btn" href="{{ route('contactus') }}"><span>Contact Us</span></a>
                        <div class="inner-image">
                            <img src="{{asset('/assets/img/bubble.png') }}" alt="">
                            <img src="{{asset('/assets/img/bubble.png') }}" alt="">
                            <img src="{{asset('/assets/img/bubble.png') }}" alt="">
                            <img src="{{asset('/assets/img/bubble.png') }}" alt="">
                            <img src="{{asset('/assets/img/bubble.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 slider banner-home">
                <img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
            </div>
        </div>
    </div>
</section>
<!-- banner section End -->
<!-- Our Courses section start -->
<section class="ti_padding_top_60 bg-white">
    <div class="container-fluid px-3">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="ti_heading_wrapper text-center">
                    <h2><span> <img src="{{asset('/assets/img/heading_icon.png') }}"></span>Our Courses</h2>
                    <p class="p-3 w-75 m-auto text-center pb-5">Choose a course and set off on an exciting journey with
                        us. The courses are modeled in the manner where you get everything covered from basics to
                        advance.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 course-card-main">
                <div class="card">
                    <div class="card-logo">
                        <img src="{{asset('/assets/img/frontend.png') }}" alt="home-course" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title text-center pt-5">
                            Front end development
                        </h5>
                        <p class="card-text text-center">
                            Development of the graphical user interface of a website or the client-facing part of a
                            website by using front end languages such as HTML-5, CSS-3, JavaScript.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 course-card-main">
                <div class="card">
                    <div class="card-logo">
                        <img src="{{asset('/assets/img/backend.png') }}" alt="home-course" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title text-center pt-5">
                            Back end development
                        </h5>
                        <p class="card-text text-center">
                            Development of the server-side of a website which mainly focuses on website architecture,
                            scripting and communication with databases.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 course-card-main">
                <div class="card">
                    <div class="card-logo">
                        <img src="{{asset('/assets/img/App-devlopment.png') }}" alt="home-course" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title text-center pt-5">
                            App development
                        </h5>
                        <p class="card-text text-center">
                            Development of computer applications of different needs and complexities for their use on
                            devices such as Tablet, Smartphone and other mobile devices.
                        </p>
                    </div>
                </div>
            </div>
            <div>
                <a href="{{ route('courses') }}" class="btn"><span>View More</span> </a>
            </div>

        </div>
    </div>
</section>
<!-- Our Courses section End -->
<!-- Our training section start -->
<section class="ti_padding_top_60 bg-white">
    <div class="container-fluid px-3">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="ti_heading_wrapper text-center">
                    <h2><span> <img src="{{asset('/assets/img/heading_icon.png') }}"></span>Our Training</h2>
                    <p class="p-3 w-75 m-auto text-center pb-5">We provide effective and quality Summer training on the
                        latest technology.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 training-section mt-4">
                <div class="card">
                    <div class="training-logo">
                        <img src="{{asset('/assets/img/App-devlopment.png') }}" alt="home-course" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Summer training</h5>
                        <p class="card-text taining-des">Techima and its well-experienced Team leaders have been designed the Summer
                            training Syllabus keeping in mind that we focus on delivering highly well-structured quality
                            content in Scheduled time </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 training-section mt-4">
                <div class="card">
                    <div class="training-logo">
                        <img src="{{asset('/assets/img/App-devlopment.png') }}" alt="home-course" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Summer training</h5>
                        <p class="card-text taining-des">Techima and its well-experienced Team leaders have been designed the Summer
                            training Syllabus keeping in mind that we focus on delivering highly well-structured quality
                            content in Scheduled time </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 training-section mt-4">
                <div class="card">
                    <div class="training-logo">
                        <img src="{{asset('/assets/img/App-devlopment.png') }}" alt="home-course" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Summer training</h5>
                        <p class="card-text taining-des">Techima and its well-experienced Team leaders have been designed the Summer
                            training Syllabus keeping in mind that we focus on delivering highly well-structured quality
                            content in Scheduled time </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 training-section mt-4">
                <div class="card">
                    <div class="training-logo">
                        <img src="{{asset('/assets/img/App-devlopment.png') }}" alt="home-course" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Summer training</h5>
                        <p class="card-text taining-des">Techima and its well-experienced Team leaders have been designed the Summer
                            training Syllabus keeping in mind that we focus on delivering highly well-structured quality
                            content in Scheduled time </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 training-section mt-4">
                <div class="card">
                    <div class="training-logo">
                        <img src="{{asset('/assets/img/App-devlopment.png') }}" alt="home-course" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Summer training</h5>
                        <p class="card-text taining-des">Techima and its well-experienced Team leaders have been designed the Summer
                            training Syllabus keeping in mind that we focus on delivering highly well-structured quality
                            content in Scheduled time </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 training-section mt-4">
                <div class="card">
                    <div class="training-logo">
                        <img src="{{asset('/assets/img/App-devlopment.png') }}" alt="home-course" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Summer training</h5>
                        <p class="card-text taining-des">Techima and its well-experienced Team leaders have been designed the Summer
                            training Syllabus keeping in mind that we focus on delivering highly well-structured quality
                            content in Scheduled time </p>
                    </div>
                </div>
            </div>
            <div class="">
                <a href="{{ route('traning') }}" class="btn"> <span>Read More</span> </a>
            </div>
        </div>
    </div>
</section>
<!-- Our training section End -->
<!-- Services section start -->
<section class="ti_padding_top_60 bg-white">
    <div class="container-fluid px-3">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="ti_heading_wrapper text-center">
                    <h2><span> <img src="{{asset('/assets/img/heading_icon.png') }}"></span> Our Services</h2>
                    <p class="p-3 w-75 m-auto text-center pb-5">Techsima has been proving to be a giant name within the field of Software and Web/App Development. We have been serving clients with not only software’s that they need but also what they want to get. </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 service-card mt-4">
                <div class="card">
                    <div class="services-logo">
                        <img src="{{asset('/assets/img/service-icon.png') }}" alt="home-service" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Software Development</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti tempore
                            fugiat beatae ipsum mollitia quas doloribus sed explicabo dolores perferendis!</p>
                        <a href="{{ route('services') }}" class="text-uppercase text-decoration-none">View More <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 service-card mt-4">
                <div class="card">
                    <div class="services-logo">
                        <img src="{{asset('/assets/img/service-icon.png') }}" alt="home-service" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Website Designing</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti tempore
                            fugiat beatae ipsum mollitia quas doloribus sed explicabo dolores perferendis!</p>
                        <a href="{{ route('services') }}" class="text-uppercase text-decoration-none">View More <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 service-card mt-4">
                <div class="card">
                    <div class="services-logo">
                        <img src="{{asset('/assets/img/service-icon.png') }}" alt="home-service" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Mobile App Development</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti tempore
                            fugiat beatae ipsum mollitia quas doloribus sed explicabo dolores perferendis!</p>
                        <a href="{{ route('services') }}" class="text-uppercase text-decoration-none">View More <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 service-card mt-4">
                <div class="card">
                    <div class="services-logo">
                        <img src="{{asset('/assets/img/service-icon.png') }}" alt="home-service" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Digital Marketing</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti tempore
                            fugiat beatae ipsum mollitia quas doloribus sed explicabo dolores perferendis!</p>
                        <a href="{{ route('services') }}" class="text-uppercase text-decoration-none">View More <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 service-card mt-4">
                <div class="card">
                    <div class="services-logo">
                        <img src="{{asset('/assets/img/service-icon.png') }}" alt="home-service" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Graphic Designing</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti tempore
                            fugiat beatae ipsum mollitia quas doloribus sed explicabo dolores perferendis!</p>
                        <a href="{{ route('services') }}" class="text-uppercase text-decoration-none">View More <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 service-card mt-4">
                <div class="card">
                    <div class="services-logo">
                        <img src="{{asset('/assets/img/service-icon.png') }}" alt="home-service" />
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Maintenance Services</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti tempore
                            fugiat beatae ipsum mollitia quas doloribus sed explicabo dolores perferendis!</p>
                        <a href="{{ route('services') }}" class="text-uppercase text-decoration-none">View More <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services section End -->
<!--  Enquery section start -->
<section class="ti_padding_top_60 bg-white">
    <div class="container">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="ti_heading_wrapper text-center">
                    <h2><span> <img src="{{asset('/assets/img/heading_icon.png') }}"></span> Customer Enquery</h2>
                    <p class="p-3 w-75 m-auto text-center pb-5">Lorem, ipsum dolor sit amet consectetur adipisicing
                        elit. Exercitationem eveniet, error et ipsa sint, minus quo nemo vitae maiores aliquam officiis,
                        eos alias ipsum! Suscipit est tenetur reprehenderit rerum natus?</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="enquery-image">
                    <img src="{{asset('/assets/img/enquery.jpg') }}">
                </div>
            </div>
            <div class="col-md-6 enquery-form">
                @if(Session::has('success'))
                <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                @endif
                <form method="post" action="{{route('enquiry')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Full name" onclick="hideError(this)">
                        @if($errors->has('name'))
                        <div class="text-danger" id="name">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                    <div class="mb-3">
                        <input type="email" class="form-control" value="{{ old('email') }}" name="email" placeholder="Enter your email" onclick="hideError(this)">
                        @if($errors->has('email'))
                        <div class="text-danger" id="email">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <div class="mb-3">
                        <input type="text" class="form-control" value="{{ old('phone') }}" name="phone" placeholder="Enter you number" onclick="hideError(this)">
                        @if($errors->has('phone'))
                        <div class="text-danger" id="phone">{{ $errors->first('phone') }}</div>
                        @endif
                    </div>
                    <div class="mb-3">
                        <textarea placeholder="Message here..." class="form-control" id="Textarea1" rows="3" name="enquiry" onclick="hideError(this)">{{ old('enquiry') }}</textarea>
                        @if($errors->has('enquiry'))
                        <div class="text-danger" id="enquiry">{{ $errors->first('enquiry') }}</div>
                        @endif
                    </div>
                    <button type="submit" class="btn d-inline"><span>Submit</span></button>
                </form>
            </div>
        </div>
    </div>
</section>
<!--  Enquery section start -->
<!-- Start testimonials section -->
<div class="p-4 mb-0  text-center bg-white ti_heading_wrapper">
    <h2 class="pt-2">Our student mostly review for this company</h2>
</div>
<section class="testimonials py-5 bg-white text-white px-1 px-md-5 margin-top-xl">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="carousel-controls testimonial-carousel-controls">
                    <div class="control d-flex align-items-center justify-content-center prev mt-3"><i class="fa fa-chevron-left"></i></div>
                    <div class="control d-flex align-items-center justify-content-center next mt-3"><i class="fa fa-chevron-right"></i></div>
                    <div class="testimonial-carousel">
                        <div class="h5 font-weight-normal one-slide mx-auto">
                            <div class="testimonial w-100 px-3 text-center d-flex flex-direction-column justify-content-center flex-wrap align-items-center">
                                <div class="test-image w-100 d-flex align-items-center justify-content-center p-4">
                                    <img src="{{asset('/assets/img/heading_icon.png') }}">
                                </div>
                                <div class="message text-center blockquote w-75">"They’ve been consistent throughout the
                                    years and grown together with us. Even as they’ve grown, they haven’t lost sight of
                                    what they do. Most of their key resources are still with them, which is also a
                                    testament to their organization."</div>
                                <div class="blockquote-name w-100">Jim Joe, WebCorpCo</div>
                            </div>
                        </div>
                        <div class="h5 font-weight-normal one-slide mx-auto">
                            <div class="testimonial w-100 px-3 text-center  d-flex flex-direction-column justify-content-center flex-wrap align-items-center">
                                <div class="test-image w-100 d-flex align-items-center justify-content-center p-4">
                                    <img src="{{asset('/assets/img/heading_icon.png') }}">
                                </div>
                                <div class="message text-center blockquote w-75">"Miami Beach Visitor and Convention
                                    Authority uses Solodev to craft a website capable of representing its diverse
                                    residents. The website features a newsroom with the latest events, an interactive
                                    calendar, and a mobile app that puts the
                                    resources of VCA at a user’s fingertips."</div>
                                <div class="blockquote-name w-100">Jim Joe, WebCorpCo</div>
                            </div>
                        </div>
                        <div class="h5 font-weight-normal one-slide mx-auto">
                            <div class="testimonial w-100 px-3 text-center  d-flex flex-direction-column justify-content-center flex-wrap align-items-center">
                                <div class="test-image w-100 d-flex align-items-center justify-content-center p-4">
                                    <img src="{{asset('/assets/img/heading_icon.png') }}">
                                </div>
                                <div class="message text-center blockquote w-75">Solodev is a great company to partner
                                    with! We are extremely happy with the software, service, and support.</div>
                                <div class="blockquote-name w-100">Jim Joe, WebCorpCo</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End testimonials section -->
<section class="ti_padding_top_60 bg-white">
    <div class="container">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-md-6">
                <h2 class="about-main-title text-uppercase newsletter">Newsletter</h2>
                <p class="pt-4">Be a part of our community by subscribing to the Techsima’s newsletter. These
                    newsletters are tailored for your prime utility and let you dive deep into the relevant and valuable
                    information. </p>
            </div>
            <div class="col-md-6">
                @if(Session::has('success'))
                <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                @endif
                <div class="search position-relative">
                    <form method="POST" action="{{ route('subscribe') }}">
                        @csrf
                        <input class="form-control rounded-pill" name='subscribe_email' onclick="hideError(this)" placeholder="Your email address">
                        <button class="btn-submit position-absolute rounded-pill">Subscribe</button>
                        @if($errors->has('subscribe_email'))
                        <div class="text-danger" id="subscribe_email">{{ $errors->first('subscribe_email') }}</div>
                        @endif
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Scroll Top Start -->
<!-- Scroll Top End -->
@endsection