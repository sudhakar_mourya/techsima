@extends('layout.home')
@section('frontdata')
<section class="error-page">
    <div class="container-fluid">
        <div class="row">
            <div class="error-page-inner">
                <h1>400</h1>
                <h2>Bad Request</h2>
                <a class="btn-error" href="{{ route('home') }}">GO BACK TO HOME</a>
            </div>            
        </div>
    </div>
</section>
@endsection