@extends('layout.home')
@section('frontdata')
<!-- banner section start -->
<section class="bg-white bg-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="slider">
                <img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
            </div>
        </div>
    </div>
</section>
<!-- banner section end -->
<section class="about-section-main bg-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 about-image">
                <img src="{{asset('assets/img/about-tech.webp') }}">
                <div class="row">
                    <div class="about-image-inner"></div>
                </div>
            </div>
            <div class="col-lg-6 about-content ps-5">
                <h2 class="about-main-title">About Us</h2>
                <p class="pt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, magni ipsam! Ad perspiciatis deleniti alias, ratione molestias illum laudantium itaque molestiae, voluptatem nam nulla eum nisi rem facilis, maiores recusandae.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim consectetur praesentium rerum rem magni distinctio dolor. Dolorum asperiores molestiae id nostrum aut eaque corrupti deleniti voluptates facilis, possimus nulla necessitatibus.</p>
            </div>
        </div>
    </div>
</section>
<section class="about-section-main bg-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 about-content ps-5">
                <h2 class="about-main-title">Our Story</h2>
                <p class="pt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, magni ipsam! Ad perspiciatis deleniti alias, ratione molestias illum laudantium itaque molestiae, voluptatem nam nulla eum nisi rem facilis, maiores recusandae.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim consectetur praesentium rerum rem magni distinctio dolor. Dolorum asperiores molestiae id nostrum aut eaque corrupti deleniti voluptates facilis, possimus nulla necessitatibus.</p>
            </div>
            <div class="col-lg-6 about-image">
                <img src="{{asset('assets/img/about-creative.jpg') }}">
            </div>
        </div>
    </div>
</section>
<section class="about-section-main bg-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 about-image">
                <img src="{{asset('assets/img/creative.jpg') }}">
            </div>
            <div class="col-lg-6 about-content ps-5">
                <h2 class="about-main-title">Creative thinking </h2>
                <p class="pt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, magni ipsam! Ad perspiciatis deleniti alias, ratione molestias illum laudantium itaque molestiae, voluptatem nam nulla eum nisi rem facilis, maiores recusandae.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim consectetur praesentium rerum rem magni distinctio dolor. Dolorum asperiores molestiae id nostrum aut eaque corrupti deleniti voluptates facilis, possimus nulla necessitatibus.</p>
            </div>
        </div>
    </div>
</section>
<!-- Start testimonials section -->
<section class="testimonials py-5 text-white px-1 px-md-5 margin-top-xl bg-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="pt-2 text-center font-weight-bold">Our Customers Are Seeing Big Results</h2>
                <div class="carousel-controls testimonial-carousel-controls">
                    <div class="control d-flex align-items-center justify-content-center prev mt-5"><i class="fa fa-chevron-left"></i></div>
                    <div class="control d-flex align-items-center justify-content-center next mt-5"><i class="fa fa-chevron-right"></i></div>

                    <div class="testimonial-carousel">
                        <div class="h5 font-weight-normal one-slide mx-auto">
                            <div class="testimonial w-100 px-5 text-center d-flex flex-direction-column justify-content-center flex-wrap align-items-center">
                                <div class="message text-center blockquote w-75">"They’ve been consistent throughout the years and grown together with us. Even as they’ve grown, they haven’t lost sight of what they do. Most of their key resources are still with them, which is also a testament to their organization."</div>
                                <div class="blockquote-footer w-100 ">Jim Joe, WebCorpCo</div>
                            </div>
                        </div>
                        <div class="h5 font-weight-normal one-slide mx-auto">
                            <div class="testimonial w-100 px-5 text-center  d-flex flex-direction-column justify-content-center flex-wrap align-items-center">
                                <div class="message text-center blockquote w-75">"Miami Beach Visitor and Convention Authority uses Solodev to craft a website capable of representing its diverse residents. The website features a newsroom with the latest events, an interactive calendar, and a mobile app that puts the
                                    resources of VCA at a user’s fingertips."</div>
                                <div class="blockquote-footer w-100 ">Jim Joe, WebCorpCo</div>
                            </div>
                        </div>
                        <div class="h5 font-weight-normal one-slide mx-auto">
                            <div class="testimonial w-100 px-5 text-center  d-flex flex-direction-column justify-content-center flex-wrap align-items-center">
                                <div class="message text-center blockquote w-75">Solodev is a great company to partner with! We are extremely happy with the software, service, and support.</div>
                                <div class="blockquote-footer w-100">Jim Joe, WebCorpCo</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End testimonials section -->
@endsection