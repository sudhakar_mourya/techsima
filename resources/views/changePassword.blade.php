@extends('layout.home')
@section('frontdata')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 forget-pwd-section bg-white">
                <h3 class="text-center pb-4 text-uppercase">Change Password</h3>
                @if(Session::has('success'))
                    <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                @endif
                @if(Session::has('error'))
                    <h6 class="text-error" id="error">{{ Session::get('error') }}</h6>
                @endif
                <form action="{{route('change-password')}}" method="post">
                    @csrf
                    <div class="mb-3">
                        <label for="current_password" class="form-label">Current Password</label>
                        <input type="text" class="form-control" name="current_password" value="{{ old('current_password') }}" onclick="hideError(this)" />
                        @if($errors->has('current_password'))
                            <div class="text-danger" id="current_password">{{ $errors->first('current_password') }}</div>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="new_password" class="form-label">New Password</label>
                        <input type="text" class="form-control" name="new_password" value="{{ old('new_password') }}" onclick="hideError(this)" />
                        @if($errors->has('new_password'))
                            <div class="text-danger" id="new_password">{{ $errors->first('new_password') }}</div>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="confirm_password" class="form-label">Confirm Password</label>
                        <input type="text" class="form-control" name="confirm_password" value="{{ old('confirm_password') }}" onclick="hideError(this)" />
                        @if($errors->has('confirm_password'))
                            <div class="text-danger" id="confirm_password">{{ $errors->first('confirm_password') }}</div>
                        @endif
                    </div>
                    <button type="submit" class="btn-training">Current Password</button>
                </form>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
@endsection