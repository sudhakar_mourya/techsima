@extends('layout.home')
@section('frontdata')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 forget-pwd-section bg-white">
                <h3 class="text-center pb-4 text-uppercase">Forget Password</h3>
                @if(Session::has('success'))
                    <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                @endif
                <form action="{{route('forget-password')}}" method="post">
                    <div class="mb-3">
                        @csrf
                        <label for="email" class="form-label">Enter Your Registered Email</label>
                        <input type="text" class="form-control" name="email" id="email" aria-describedby="emailHelp">
                        @if($errors->has('email'))
                            <div class="text-danger" id="email">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <button type="submit" class="btn-training">Reset Link</button>
                </form>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
@endsection