@extends('layout.home')
@section('frontdata')
<section class="sitemap-main">
    <div class="container">
        <h2 class="text-center text-uppercase fw-bold" style="color: #002b2e;">SiteMap</h2>
        <div class="siteMap">
            <a href=""><i class="fas fa-angle-double-right fw-light"></i><span class="ps-2">About Us</span></a>
            <a href=""><i class="fas fa-angle-double-right fw-light"></i><span class="ps-2">Traning</span></a>
            <a href=""><i class="fas fa-angle-double-right fw-light"></i><span class="ps-2">Registration</span></a>
            <a href=""><i class="fas fa-angle-double-right fw-light"></i><span class="ps-2">Contact Us</span></a>
            <a href=""><i class="fas fa-angle-double-right fw-light"></i><span class="ps-2">Faq</span></a>
            <a href=""><i class="fas fa-angle-double-right fw-light"></i><span class="ps-2">Course</span></a>
            <a href=""><i class="fas fa-angle-double-right fw-light"></i><span class="ps-2">Services</span></a>
            <a href=""><i class="fas fa-angle-double-right fw-light"></i><span class="ps-2">Gallary</span></a>
            <a href=""><i class="fas fa-angle-double-right fw-light"></i><span class="ps-2">Privacy Policy</span></a>
            <a href=""><i class="fas fa-angle-double-right fw-light"></i><span class="ps-2">Login</span></a>
        </div>
    </div>
</section>
@endsection