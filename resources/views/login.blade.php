@extends('layout.home')
@section('frontdata')
<!-- banner section start -->
<section class="bg-white bg-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="slider">
                <img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
            </div>
        </div>
    </div>
</section>
<!-- banner section end -->
<section class="ti_padding_top_60">
    <div class="container">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 login p-5">
                @if(Session::has('success'))
                <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                @endif
                @if(Session::has('danger'))
                <h6 class="text-danger" id="danger">{{ Session::get('danger') }}</h6>
                @endif
                <h2 class="form-title">LogIn</h2>
                <form method="post" action="{{route('login')}}">
                    @csrf
                    <div class="mb-3">
                        <label for="InputEmail1" class="form-label">Email address</label>
                        <input type="text" class="form-control" name="email" id="InputEmail1" onclick="hideError(this)" aria-describedby="emailHelp">
                        @if($errors->has('email'))
                        <div class="text-danger" id="email">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="InputPassword1" class="form-label">Password</label>
                        <input type="text" class="form-control" name="password" onclick="hideError(this)" id="InputPassword1">
                        @if($errors->has('password'))
                        <div class="text-danger" id="password">{{ $errors->first('password') }}</div>
                        @endif
                    </div>
                    <div class="d-flex justify-content-between pt-3">
                        <a href="{{route('forget-password')}}">Forgot password?</a>
                        <button type="submit" class="btn-training">Submit</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-3"></div>
        </div>
    </div>
</section>
@endsection