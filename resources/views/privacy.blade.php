@extends('layout.home')
@section('frontdata')
<!-- banner section start -->
<section class="bg-white bg-banner">
	<div class="container-fluid">
		<div class="row">
			<div class="slider">
				<img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
			</div>
		</div>
	</div>
</section>
<!-- banner section end -->
<!--Start Privacy Policy Section -->
<section class="privacy-main">
	<div class="container">
		<div class="row">
			<h2 class="text-center text-uppercase">Privacy Policy</h2>
			<p class="pt-5 ">We respect your privacy and it is important to us and we are committed to safeguarding any personal identifiable information you may provide us through the use of this website. This privacy policy explains what information Techsima Solutions Private Limited collect about you, why Techsima Solutions Private Limited collect your information, and how Techsima Solutions Private Limited collect and processes your information and for what purposes. We use your data to provide and improve our services to you. By using Service, users agree to the collection and use of their personal data as stated in this Privacy Policy.
				This Privacy Policy is important. We hope you will take time to read it carefully.
			</p>
			<h3>Table of Content (links attached for following)</h3>
			<ul class="ps-5">
				<li>Who are we?</li>
				<li>How we collect data by different ways</li>
				<li>What we do with the data collected</li>
				<li>Cookies</li>
				<li>Security</li>
				<li>Changes to Privacy Policy</li>
				<li>Contact</li>
			</ul>
			<h3>Who are we?</h3>
			<p class="ps-5">In this Privacy Policy, any mention of “we”, “our” or “us” shall mean Techsima Solutions Private Limited and/or its affiliated organizations. Any mention of “you” or “your” are shall indicate the user of the site and service.
				Techsima Solutions Private Limited and its affiliated operations are responsible for the control of your personal information. Techsima Solutions Private Limited has its main place of business located at Ayodhya, Uttar Pradesh.
			</p>
			<h3>How we collect data by different ways</h3>
			<p class="ps-5">We collect your data through our interactions with you, and through our services. We collect your personal data (such as the User’s name, address, email address, mobile number, age and gender) while you register on our website or when you subscribe to any services provided by us. We also collect your data while you make a purchase of any item/service(s). We may also collect your data from Third Parties to whom you have provided your information with your consent to share it to other organizations. We also collect personally identifiable information such as Internet Protocol (IP) addresses, unique browser identifiers, browser type, browser version, and the pages of our Service you visit, the time and date of your visit,
				the time spent on those pages and other device specific information from our logged in users.
			</p>
			<h3>What we do with your information</h3>
			<p class="ps-5">We use your data to identify you when you use our website or services. We also use your data to provide and maintain our services to you; to notify about changes; to detect, prevent and address any technical issues; for customer support and better customer experience and for any other purposes with your consent.
				To provide the best of our services to you, we may use your data for further usage such as email marketing, telemarketing and to send promotional material and newsletters.
				Techsima Solutions Private Limited can also use specific personal information in case of a response to a court order, search warrant, subpoena, law enforcement proceedings, regulatory inquiries and other legal obligations.</p>
			<h3>Cookies</h3>
			<p class="ps-5">Cookies are usually small piece of data (which typically includes a computer code) that the server sends to the user’s web browser. We use Cookies for User personalization which includes user preferences and settings and for analysing our services to you and for fulfilling other purposes with your consent. It helps us to improve the website functionality for you and customize our services according to your preferences and interest. You have a choice to refuse cookies from your browser, but it may result in not being able to use some of our services.</p>
			<h3>Security</h3>
			<p class="ps-5">We maintain reasonable security measures to safeguard your personal data against unauthorised access. We put our best efforts in securing the personal data during transmission over the internet and in storing it electronically, but we cannot always ensure absolute security of any information that you transmit to us. You agree that you provide your information and engage in such transmissions at your own risk. We keep assessing our security measures, data privacy and information management frequently and strive to keep your information as secure as possible. </p>
			<h3>Changes to Privacy Policy</h3>
			<p class="ps-5">We may update Privacy Policy time to time and if we do we’ll notify you about the changes by posting them on this page. If you keep using our website and services after the changes are in effect (which is when the changes are posted on the Privacy Policy page), you automatically agree to the changes. You are advised to keep reviewing the Privacy Policy to keep yourself updated with the changes.
				Techsima Solutions Private Limited has the final say in any case of dispute. Techsima Solutions Private Limited keeps the right to change the terms and conditions and Privacy Policy.</p>
			<h3>Contact</h3>
			<p class="ps-5">You can contact us on info@techsima.com if you have any query regarding the Privacy Policy. You can also contact us on the given phone number- 7800050976, 8090995797. If you are a resident of Ayodhya, you can visit our office premises at address, Ayodhya.</p>
		</div>
	</div>
</section>

<!--End Privacy Policy Section -->
@endsection