@extends('layout.home')
@section('frontdata')
<!-- banner section start -->
<section class="bg-white bg-banner">
	<div class="container-fluid">
		<div class="row">			
			<div class="slider">
                <img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
			</div>
		</div>
	</div>
</section>
<!-- banner section end -->
<section class="ti_section ti_feature_section ti_padding_top_60 bg-white demo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ti_heading_wrapper text-center">
					<h2>Our Courses</h2>
					<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Exercitationem eveniet, error et ipsa sint, minus quo nemo vitae maiores aliquam officiis, eos alias ipsum! Suscipit est tenetur reprehenderit rerum natus?</p>
				</div>
			</div>
			<h3 class="course-heading">FrontEnd Devlopment</h3>
			@foreach($frontend as $item)
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow bounceInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: bounceInLeft;">
				<div class="ti_feature_box text-center block" style="visibility: visible;">
					<div class="ti_feature_icon">
						<img src="{{ env('FILE_URL').$item->logo }}" class="img-fluid">
					</div>
					<h4 class="ti_subheading">{{$item->name}}</h4>
				</div>
			</div>
			@endforeach		
		</div>
	</div>
	<div class="container">
		<div class="row">
			<h3 class="course-heading">BackEnd Devlopment</h3>
			@foreach($backend as $item)
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow bounceInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: bounceInLeft;">
				<div class="ti_feature_box text-center block" style="visibility: visible;">
					<div class="ti_feature_icon">
						<img src="{{ env('FILE_URL').$item->logo }}" class="img-fluid">
					</div>
					<h4 class="ti_subheading">{{$item->name}}</h4>
				</div>
			</div>	
			@endforeach			
		</div>
	</div>
	<div class="container">
		<div class="row">
			<h3 class="course-heading">MobileApp Devlopment</h3>
			@foreach($mobile as $item)
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow bounceInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: bounceInLeft;">
				<div class="ti_feature_box text-center block" style="visibility: visible;">
					<div class="ti_feature_icon">
						<img src="{{ env('FILE_URL').$item->logo }}" class="img-fluid">
					</div>
					<h4 class="ti_subheading">{{$item->name}}</h4>
				</div>
			</div>	
			@endforeach		
		</div>
	</div>
</section>
@endsection