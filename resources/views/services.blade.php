@extends('layout.home')
@section('frontdata')
<!-- banner section start -->
<section class="bg-white bg-banner">
	<div class="container-fluid">
		<div class="row">
			<!-- <div class="col-lg-6 background-slider">
				<div class="row background-slider-outer">				
                    <div class="breadcrumb-section d-flex justify-content-center align-items-center">
                        <nav aria-label="breadcrumb ">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Library</li>
                            </ol>
                        </nav>
                    </div>
				</div>
			</div> -->
			<div class="slider">
                <img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
			</div>
		</div>
	</div>
</section>
<!-- banner section end -->
<!-- Services section start -->
<section class="ti_padding_top_60 bg-white">
	<div class="container px-3">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ti_heading_wrapper text-center">
					<h2> Our Services</h2>
					<p class="p-3 w-75 m-auto text-center pb-5">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Exercitationem eveniet, error et ipsa sint, minus quo nemo vitae maiores aliquam officiis, eos alias ipsum! Suscipit est tenetur reprehenderit rerum natus?</p>
				</div>
			</div>
			@foreach($service as $item)
			<div class="col-lg-4 col-md-6 col-sm-12 service-section">
				<div class="card">
				<div class="services-logo">
						<img src="{{ env('FILE_URL').$item->image }}">
					</div>
					<div class="service-section-inner">
					<h5 class="card-title">{{$item->name}}</h5>
					<p class="card-text">{{$item->short_description}}</p>
					<a href="{{ route('servicedetail', $item->id) }}"class="btn-training text-uppercase text-decoration-none">View More  </a>
				</div>
				</div>
			</div>	
			@endforeach			
		</div>
	</div>
</section>
<!-- Services section End -->
@endsection