@extends('layout.home')

@section('frontdata')
<!-- banner section start -->
<section class="bg-white bg-banner">
	<div class="container-fluid">
		<div class="row">
			<div class="slider">
                <img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
			</div>
		</div>
	</div>
</section>
<!-- banner section end -->
<section class="ContactUs ti_padding_top_60">
    <div class="container">
        <div class="contactinfo">
            <div>
                <h2> Contact Info</h2>
                <ul class="info">
                    <li>
                        <span><i class="fas fa-location-arrow"></i></span>
                        <span>In front of, Rajarshi Dashrath Rajkiya Medical College, kushmaha, Faizabad Rd, Ayodhya, Uttar Pradesh 224135.</span>
                    </li>
                    <li>
                        <span><i class="fas fa-envelope"></i></span>
                        <span><a href="mailto:info@techsima.com">info@techsima.com</a></span>
                    </li>
                    <li>
                        <span><i class="fas fa-mobile-alt"></i></span>
                        <span><a href="tel:+91 7800050976">+91 7800050976 </a><br/><a href="tel:+91 8090995797">+91 8090995797</a></span>
                    </li>
                </ul>
            </div>
            <ul class="icon">
                <li><a href="javascript:void(0);"> <i class="fab fa-youtube"></i></a></li>
                <li><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                <li><a href="javascript:void(0);"><i class="fab fa-pinterest-p"></i></a></li>
                <li><a href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a></li>
            </ul>
        </div>
        <div class="contactform">
            @if(Session::has('success'))
                <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
            @endif
            <h2>Send a Message</h2>
            <form role="form" method="post" action="{{route('contactus')}}" enctype="multipart/form-data">
                @csrf
                <div class="FormBox">
                    <div class="inputbox w50">
                        <input type="text" name="name"  value="{{ old('name') }}" onclick="hideError(this)"/>
                        <span>First Name</span>
                        @if($errors->has('name'))
                            <div class="text-danger" id="name">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                    <div class="inputbox w50">
                        <input type="text" name="last_name" value="{{ old('last_name') }}" onclick="hideError(this)"/>
                        <span>Last Name</span>
                        @if($errors->has('last_name'))
                            <div class="text-danger" id="last_name">{{ $errors->first('last_name') }}</div>
                        @endif
                    </div>
                    <div class="inputbox w50">
                        <input type="email" name="email" value="{{ old('email') }}" onclick="hideError(this)"/>
                        <span>Email Address</span>
                        @if($errors->has('email'))
                            <div class="text-danger" id="email">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <div class="inputbox w50">
                        <input type="text" value="{{ old('phone') }}" onclick="hideError(this)" name="phone"/>
                        <span>phone Number</span>
                        @if($errors->has('phone'))
                            <div class="text-danger" id="phone">{{ $errors->first('phone') }}</div>
                        @endif
                    </div>
                    <div class="inputbox w100">
                        <textarea name="message" onclick="hideError(this)">{{ old('message') }}</textarea>
                        <span>Write your message here...</span>
                        @if($errors->has('message'))
                            <div class="text-danger" id="message">{{ $errors->first('message') }}</div>
                        @endif
                    </div>
                    <div class="inputbox w100">
                        <input type="submit" value="Send">
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 map p-3">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1781.2833816846867!2d82.20146705805617!3d26.758197995798657!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399a07d8eb5fd27f%3A0xcfbbc5e4bb9f9b2d!2sTechsima%20Solution%20Private%20Limited!5e0!3m2!1sen!2sin!4v1647951898058!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </div>
</section>
@endsection