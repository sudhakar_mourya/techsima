@extends('layout.home')
@section('frontdata')
<!-- banner section start -->
<section class="bg-white bg-banner">
	<div class="container-fluid">
		<div class="row">
			<!-- <div class="col-lg-6 background-slider">
				<div class="row background-slider-outer">				
                    <div class="breadcrumb-section d-flex justify-content-center align-items-center">
                        <nav aria-label="breadcrumb ">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Library</li>
                            </ol>
                        </nav>
                    </div>
				</div>
			</div> -->
			<div class="slider">
                <img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
			</div>
		</div>
	</div>
</section>
<!-- banner section end -->
@endsection