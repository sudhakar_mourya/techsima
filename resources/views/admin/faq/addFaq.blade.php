@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section class="bg-white">
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                @if(Session::has('success'))
                                    <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                                @endif
                                <h6>Faq</h6>
                                <button class="btn btn-secondary addBtn"><a href="{{ route('view', 'faq') }}">Faq List</a></button>
                            </div>
                            <div class="card-body">
                                <form role="form" method="post" action="{{route('addFaq')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="FormControlTextarea1" class="form-label">Question</label>
                                        <textarea class="form-control" name="question" onclick="hideError(this)" id="FormControlTextarea1" rows="3">{{ old('question') }}</textarea>
                                        @if($errors->has('question'))
                                            <div class="text-danger" id="question">{{ $errors->first('question') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="FormControlTextarea1" class="form-label">Answer</label>
                                        <textarea class="form-control" name="answer" onclick="hideError(this)" id="FormControlTextarea1" rows="3">{{ old('answer') }} </textarea>
                                        @if($errors->has('answer'))
                                            <div class="text-danger" id="answer">{{ $errors->first('answer') }}</div>
                                        @endif
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Uploade Question</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection