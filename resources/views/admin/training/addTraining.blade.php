@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section>
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                @if(Session::has('success'))
                                    <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                                @endif
                                <h6>Add Training</h6>
                                <button class="btn btn-secondary addBtn"><a href="{{ route('view', 'training') }}">Training List</a></button>
                            </div>
                            <div class="card-body">
                                <form role="form" method="post" action="{{route('addTraining')}}" enctype="multipart/form-data">
                                    @csrf
                                    <label>Training Name</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name" value="{{ old('name') }}" name="name" onclick="hideError(this)" aria-label="Name" aria-describedby="email-addon">
                                        @if($errors->has('name'))
                                            <div class="text-danger" id="name">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                    <label>Training Title</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name" value="{{ old('title') }}" name="title" onclick="hideError(this)" aria-label="Name" aria-describedby="email-addon">
                                        @if($errors->has('title'))
                                            <div class="text-danger" id="title">{{ $errors->first('title') }}</div>
                                        @endif
                                    </div>
                                    <label>Image</label>
                                    <div class="mb-3">
                                        <input type="file" class="form-control" name="image" onclick="hideError(this)" aria-label="File" aria-describedby="password-addon">
                                        @if($errors->has('image'))
                                            <div class="text-danger" id="image">{{ $errors->first('image') }}</div>
                                        @endif
                                    </div>
                                    <label>Short Description</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name" value="{{ old('short_description') }}" name="short_description" onclick="hideError(this)" aria-label="Name" aria-describedby="email-addon">
                                        @if($errors->has('short_description'))
                                            <div class="text-danger" id="short_description">{{ $errors->first('short_description') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="FormControlTextarea1" class="form-label">Description</label>
                                        <textarea class="form-control" name="description" onclick="hideError(this)" id="FormControlTextarea1" rows="3">{{ old('description') }}</textarea>
                                        @if($errors->has('description'))
                                            <div class="text-danger" id="description">{{ $errors->first('description') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="FormControlTextarea1" class="form-label">Long Description</label>
                                        <textarea class="form-control" name="long_description" onclick="hideError(this)" id="FormControlTextarea1" rows="3">{{ old('long_description') }}</textarea>
                                        @if($errors->has('long_description'))
                                            <div class="text-danger" id="long_description">{{ $errors->first('long_description') }}</div>
                                        @endif
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection