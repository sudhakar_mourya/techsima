@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section>
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                @if(Session::has('success'))
                                    <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                                @endif
                                <h6>Add Poster</h6>
                                <button class="btn btn-secondary addBtn"><a href="{{ route('view', 'poster') }}">Poster List</a></button>
                            </div>
                            <div class="card-body">
                                <form role="form" method="post" action="{{route('addPoster')}}" enctype="multipart/form-data">
                                    @csrf
                                    <label>Paster Name</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name" value="{{ old('name') }}" name="name" onclick="hideError(this)" aria-label="Name" aria-describedby="email-addon">
                                        @if($errors->has('name'))
                                            <div class="text-danger" id="name">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                    <label>Page Name</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Page Name" value="{{ old('page_name') }}" name="page_name" onclick="hideError(this)" aria-label="Email" aria-describedby="email-addon">
                                        @if($errors->has('page_name'))
                                            <div class="text-danger" id="page_name">{{ $errors->first('page_name') }}</div>
                                        @endif
                                    </div>
                                    <label>Poster</label>
                                    <div class="mb-3">
                                        <input type="file" class="form-control" name="poster" onclick="hideError(this)" aria-label="File" aria-describedby="password-addon">
                                        @if($errors->has('poster'))
                                            <div class="text-danger" id="poster">{{ $errors->first('poster') }}</div>
                                        @endif
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection