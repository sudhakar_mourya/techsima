@extends('layout.admin')
@section('admindata')
<div class="container-fluid py-4">
  <div class="row">
    <div class="col-12">
      <div class="card mb-4">
        <div class="card-header pb-0">
          @if(Session::has('success'))
            <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
          @endif
          <h6>Data Table</h6>
          <button class="btn btn-secondary addBtn"><a href="{{ route('add', 'poster') }}">Add Poster</a></button>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
          <div class="table-responsive p-0">
            <table class="table align-items-center mb-0">
              <thead>
                <tr>
                  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">#Id</th>
                  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Name</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">Page Name</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">Image</th>
                  <th class="text-secondary opacity-7">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($records as $poster)
                  <tr>
                    <td class="align-middle">
                      <p class="text-xs font-weight-bold mb-0">{{ $poster->id }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $poster->name }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <span class="text-xs font-weight-bold mb-0">{{ $poster->page_name }}</span>
                    </td>
                    <td class="align-middle text-center">
                      <img src="{{ env('FILE_URL').$poster->poster }}" class="avatar avatar-sm me-3" alt="user1">
                    </td>
                    <td class="align-middle">
                      <a href="{{ route('edit', ['poster', $poster->id]) }}" class="text-secondary font-weight-bold text-xs ms-2 badge badge-sm bg-gradient-danger" data-toggle="tooltip" data-original-title="Edit user">
                        Edit
                      </a>
                      <a href="javascript:;" class="text-secondary font-weight-bold text-xs badge badge-sm bg-gradient-warning" data-toggle="tooltip" data-original-title="Edit user">
                        <form method="POST" action="{{ route('delete', ['poster', $poster->id]) }}">
                          @csrf
                          <input type="submit" class="d-sm-inline d-none" value="Delete">
                        </form>
                      </a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection