@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section class="bg-white">
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                @if(Session::has('success'))
                                    <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                                @endif
                                <h6>Add Blog</h6>
                                <button class="btn btn-secondary addBtn"><a href="{{ route('view', 'blog') }}">Blog List</a></button>
                            </div>
                            <div class="card-body">
                                <form role="form" method="post" action="{{route('editBlog', $record->id)}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="formFileMultiple" class="form-label">Photo</label>
                                        <input class="form-control" type="file" id="formFileMultiple" name="image" onclick="hideError(this)" multiple>
                                        @if($errors->has('image'))
                                            <div class="text-danger" id="image">{{ $errors->first('image') }}</div>
                                        @endif
                                    </div>
                                    <label>Heading</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" name="heading" onclick="hideError(this)" value="{{ $record->heading }}" placeholder="Heading" aria-label="Heading" aria-describedby="heading-addon">
                                        @if($errors->has('heading'))
                                            <div class="text-danger" id="heading">{{ $errors->first('heading') }}</div>
                                        @endif
                                    </div>
                                    <label>Title</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" name="title"  onclick="hideError(this)" placeholder="Title"  value="{{ $record->title }}" aria-label="Title" aria-describedby="title-addon">
                                        @if($errors->has('title'))
                                            <div class="text-danger" id="title">{{ $errors->first('title') }}</div>
                                        @endif
                                    </div>
                                    <label>Sub Title</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" name="sub_title"  onclick="hideError(this)" placeholder="Title"  value="{{ $record->sub_title }}" aria-label="Title" aria-describedby="title-addon">
                                        @if($errors->has('sub_title'))
                                            <div class="text-danger" id="sub_title">{{ $errors->first('sub_title') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="FormControlTextarea1" class="form-label">Description</label>
                                        <textarea class="form-control" id="FormControlTextarea1" name="description" onclick="hideError(this)" rows="3">{{ $record->description }}</textarea>
                                        @if($errors->has('description'))
                                            <div class="text-danger" id="description">{{ $errors->first('description') }}</div>
                                        @endif
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Send Data</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection