@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section>
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                @if(Session::has('success'))
                                    <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                                @endif
                                <h6>Add Course</h6>
                                <button class="btn btn-secondary addBtn"><a href="{{ route('view', 'course') }}">Poster List</a></button>
                            </div>
                            <div class="card-body">
                                <form role="form" method="post" action="{{route('addCourse')}}" enctype="multipart/form-data">
                                    @csrf
                                    <label>Course Name</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name" value="{{ old('name') }}" name="name" onclick="hideError(this)" aria-label="Name" aria-describedby="email-addon">
                                        @if($errors->has('name'))
                                            <div class="text-danger" id="name">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                    <label>Title</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name" value="{{ old('title') }}" name="title" onclick="hideError(this)" aria-label="Name" aria-describedby="email-addon">
                                        @if($errors->has('title'))
                                            <div class="text-danger" id="title">{{ $errors->first('title') }}</div>
                                        @endif
                                    </div>
                                    <label>Sub Title</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name" value="{{ old('sub_title') }}" name="sub_title" onclick="hideError(this)" aria-label="Name" aria-describedby="email-addon">
                                        @if($errors->has('sub_title'))
                                            <div class="text-danger" id="sub_title">{{ $errors->first('sub_title') }}</div>
                                        @endif
                                    </div>
                                    <label>Author Name</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name" value="{{ old('author_name') }}" name="author_name" onclick="hideError(this)" aria-label="Name" aria-describedby="email-addon">
                                        @if($errors->has('author_name'))
                                            <div class="text-danger" id="author_name">{{ $errors->first('author_name') }}</div>
                                        @endif
                                    </div>
                                    <label>Course Category</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name" value="{{ old('category') }}" name="category" onclick="hideError(this)" aria-label="Name" aria-describedby="email-addon">
                                        @if($errors->has('category'))
                                            <div class="text-danger" id="category">{{ $errors->first('category') }}</div>
                                        @endif
                                    </div>
                                    <label>Date</label>
                                    <div class="mb-3">
                                        <input type="date" class="form-control" placeholder="Name" value="{{ old('date') }}" name="date" onclick="hideError(this)" aria-label="Name" aria-describedby="email-addon">
                                        @if($errors->has('date'))
                                            <div class="text-danger" id="date">{{ $errors->first('date') }}</div>
                                        @endif
                                    </div>
                                    <label>Logo</label>
                                    <div class="mb-3">
                                        <input type="file" class="form-control" name="logo" onclick="hideError(this)" aria-label="File" aria-describedby="password-addon">
                                        @if($errors->has('logo'))
                                            <div class="text-danger" id="logo">{{ $errors->first('logo') }}</div>
                                        @endif
                                    </div>
                                    <label>Image</label>
                                    <div class="mb-3">
                                        <input type="file" class="form-control" name="image" onclick="hideError(this)" aria-label="File" aria-describedby="password-addon">
                                        @if($errors->has('image'))
                                            <div class="text-danger" id="image">{{ $errors->first('image') }}</div>
                                        @endif
                                    </div>
                                    <label>Description</label>
                                    <div class="mb-3">
                                        <textarea class="form-control" name="description" onclick="hideError(this)" id="FormControlTextarea1" rows="3">{{ old('description') }} </textarea>
                                        @if($errors->has('description'))
                                            <div class="text-danger" id="description">{{ $errors->first('description') }}</div>
                                        @endif
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection