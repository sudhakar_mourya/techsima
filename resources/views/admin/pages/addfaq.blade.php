@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section class="bg-white">
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                <h6>Faq</h6>
                                <button class="btn btn-secondary addBtn">Add Faq</button>
                            </div>
                            <div class="card-body">
                                <form role="form">
                                    <div class="mb-3">
                                        <label for="FormControlTextarea1" class="form-label">Question</label>
                                        <textarea class="form-control" id="FormControlTextarea1" rows="3"></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label for="FormControlTextarea1" class="form-label">Answer</label>
                                        <textarea class="form-control" id="FormControlTextarea1" rows="3"></textarea>
                                    </div>
                                    <div class="text-center">
                                        <button type="button" class="btn bg-gradient-info w-100 mt-4 mb-0">Uploade Question</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection