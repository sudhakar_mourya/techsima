@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section class="bg-white">
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                <h6>Testimonial</h6>
                                <button class="btn btn-secondary addBtn">Add Testimonial</button>
                            </div>
                            <div class="card-body">
                                <form role="form">
                                    <label>Desination</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="name-addon">
                                    </div>
                                    <label>Name</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name" aria-label="Email" aria-describedby="email-addon">
                                    </div>
                                    <div class="mb-3">
                                        <label for="FormControlTextarea1" class="form-label">Review</label>
                                        <textarea class="form-control" id="FormControlTextarea1" rows="3"></textarea>
                                    </div>

                                    <div class="mb-3">
                                        <label for="formFileMultiple" class="form-label">Photo</label>
                                        <input class="form-control" type="file" id="formFileMultiple" multiple>
                                    </div>
                                    <div class="text-center">
                                        <button type="button" class="btn bg-gradient-info w-100 mt-4 mb-0">Send Data</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection