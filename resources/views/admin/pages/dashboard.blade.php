@extends('layout.admin')

@section('admindata')
<div class="container-fluid py-4">
  <div class="row mt-4">
    <div class="col-lg-3 mb-lg-0 mb-4">
      <div class="card">
        <div class="ms-auto text-center mt-5 mt-lg-0">
          <div class="bg-gradient-primary border-radius-lg h-100">
            <h2>Poster</h2>
            <div class="position-relative d-flex align-items-center justify-content-center h-50">
              <img class="w-100 position-relative z-index-2 pt-4" src="../assets/img/illustrations/rocket-white.png" alt="rocket">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 mb-lg-0 mb-4">
      <div class="card">
        <div class="ms-auto text-center mt-5 mt-lg-0">
          <div class="bg-gradient-primary border-radius-lg h-100">
             <h2>Contact</h2>
            <div class="position-relative d-flex align-items-center justify-content-center h-100">
              <img class="w-100 position-relative z-index-2 pt-4" src="../assets/img/illustrations/rocket-white.png" alt="rocket">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 mb-lg-0 mb-4">
      <div class="card">
        <div class="ms-auto text-center mt-5 mt-lg-0">
          <div class="bg-gradient-primary border-radius-lg h-100">
            <h2>User</h2>
            <div class="position-relative d-flex align-items-center justify-content-center h-100">
              <img class="w-100 position-relative z-index-2 pt-4" src="../assets/img/illustrations/rocket-white.png" alt="rocket">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 mb-lg-0 mb-4">
      <div class="card">
        <div class="ms-auto text-center mt-5 mt-lg-0">
          <div class="bg-gradient-primary border-radius-lg h-100">
             <h2>Subscribers</h2>
            <div class="position-relative d-flex align-items-center justify-content-center h-100">
              <img class="w-100 position-relative z-index-2 pt-4" src="../assets/img/illustrations/rocket-white.png" alt="rocket">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection