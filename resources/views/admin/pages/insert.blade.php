@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section>
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                <h6>Edit Data</h6>
                                <button class="btn btn-secondary addBtn">List Button</button>
                            </div>
                            <div class="card-body">
                                <form role="form">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-sm-12  mb-3">
                                            <label>Nmae</label>
                                            <input type="text" class="form-control" placeholder="Name" aria-label="Email" aria-describedby="email-addon">
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12  mb-3">
                                            <label>Nmae</label>
                                            <input type="text" class="form-control" placeholder="Name" aria-label="Email" aria-describedby="email-addon">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-sm-12  mb-3">
                                            <label>Nmae</label>
                                            <input type="text" class="form-control" placeholder="Name" aria-label="Email" aria-describedby="email-addon">
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12  mb-3">
                                            <label>Nmae</label>
                                            <input type="text" class="form-control" placeholder="Name" aria-label="Email" aria-describedby="email-addon">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-8 col-md-12  mb-3">
                                            <label>Nmae</label>
                                            <input type="text" class="form-control" placeholder="Name" aria-label="Email" aria-describedby="email-addon">
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 mb-3">
                                            <label>Nmae</label>
                                            <input type="text" class="form-control" placeholder="Name" aria-label="Email" aria-describedby="email-addon">
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="FormControlTextarea1" class="form-label">Description</label>
                                        <textarea class="form-control" id="FormControlTextarea1" rows="3"></textarea>
                                    </div>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="rememberMe" checked="">
                                        <label class="form-check-label" for="rememberMe">Remember me</label>
                                    </div>
                                    <div class="text-center">
                                        <button type="button" class="btn bg-gradient-info w-100 mt-4 mb-0">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection