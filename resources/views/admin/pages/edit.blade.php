@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section class="bg-white">
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                <h6>Edit Data</h6>
                                <button class="btn btn-secondary addBtn">List Button</button>
                            </div>
                            <div class="card-body">
                                <form role="form">
                                    <label>Nmae</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name" aria-label="Email" aria-describedby="email-addon">
                                    </div>
                                    <label>Email</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Email" aria-label="Password" aria-describedby="password-addon">
                                    </div>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="rememberMe" checked="">
                                        <label class="form-check-label" for="rememberMe">Remember me</label>
                                    </div>
                                    <div class="text-center">
                                        <button type="button" class="btn bg-gradient-info w-100 mt-4 mb-0">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection