@extends('layout.admin')
@section('admindata')
<div class="container-fluid py-4">
  <div class="row">
    <div class="col-12">
      <div class="card mb-4">
        <div class="card-header pb-0">
          @if(Session::has('success'))
            <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
          @endif
          <h6>Data Table</h6>
          <form class="row g-3">
            <div class="col-auto">
                <select name="student" class="form-control-plaintext">
                    <option value="">select student name</option>
                    @if($student)
                    @foreach($student as $id => $name)
                    <option value="{{ $id }}" {{ isset($_GET['student']) && $_GET['student'] == $id ? 'selected' : '' }}>{{ $name }}</option>
                    @endforeach
                    @endif
                </select>
            </div>
            <div class="col-auto">
                <select name="topic" class="form-control-plaintext">
                    <option value="">select topic</option>
                    @if($topic)
                    @foreach($topic as $key => $value)
                    <option value="{{ $value }}" {{ isset($_GET['topic']) && $_GET['topic'] == $value ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                    @endif
                </select>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-sm btn-primary mb-3">Go</button>
            </div>
            </form>
</div>
        <div class="card-body px-0 pt-0 pb-2">
          <div class="table-responsive p-0">
            <table class="table align-items-center mb-0">
              <thead>
                <tr>
                  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">#Id</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">Student</th>
                  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Topic</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">Question</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">Answer</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">Attend At</th>
                </tr>
              </thead>
              <tbody>
                @foreach($records as $item)
                  <tr>
                  <td class="align-middle">
                      <p class="text-xs font-weight-bold mb-0">{{ $item->id }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <span class="text-xs font-weight-bold mb-0">{{ $item->student }}</span>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $item->topic }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <span class="text-xs font-weight-bold mb-0">{{ $item->question }}</span>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <span class="text-xs font-weight-bold mb-0">{{ $item->answer }}</span>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <span class="text-xs font-weight-bold mb-0">{{ $item->date }}</span>
                    </td>
                  </tr>
                  @endforeach 
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection