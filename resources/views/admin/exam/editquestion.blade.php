@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section class="bg-white">
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                @if(Session::has('success'))
                                    <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                                @endif
                                <h6>Exam Question</h6>
                                <button class="btn btn-secondary addBtn"><a href="{{route('questionList')}}">Question Answer List</a></button>
                            </div>
                            <div class="card-body">
                                <form role="form" method="post" action="{{route('editQuestion', ['id' => $question->id])}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="topic" class="form-label">Topic</label>
                                        <input type="text" class="form-control" name="topic"  value="@if(old('topic')) {{old('topic')}} @else {{$question->topic }} @endif" onclick="hideError(this)" />
                                        @if($errors->has('topic'))
                                            <div class="text-danger" id="topic">{{ $errors->first('topic') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="qustionentity" class="form-label">Question</label>
                                        <textarea class="form-control" name="question" id="qustionentity" rows="2" onclick="hideError(this)" >@if(old('question')) {{old('question')}} @else {{ $question->question }} @endif</textarea>
                                        @if($errors->has('question'))
                                            <div class="text-danger" id="question">{{ $errors->first('question') }}</div>
                                        @endif
                                    </div>
                                    @foreach($question->answer as $item)
                                    <div class="mb-3">
                                        <label for="opt{{$loop->iteration}}" class="form-label">option {{$loop->iteration}}</label>
                                        <textarea class="form-control"  name="option{{$loop->iteration}}" rows="1" onclick="hideError(this)" >@if(old("option$loop->iteration")) {{old("option$loop->iteration")}} @else {{ $item->answer }} @endif</textarea>
                                        @if($errors->has("option{{$loop->iteration}}"))
                                            <div class="text-danger" id="option1">{{ $errors->first("option$loop->iteration") }}</div>
                                        @endif
                                    </div>
                                    @endforeach
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Uploade Question</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection