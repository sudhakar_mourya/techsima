@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section class="bg-white">
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                @if(Session::has('success'))
                                    <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                                @endif
                                <h6>Exam Question</h6>
                                <button class="btn btn-secondary addBtn"><a href="{{route('questionList')}}">Question Answer List</a></button>
                            </div>
                            <div class="card-body">
                                <form role="form" method="post" action="{{route('addQuestion')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="topic" class="form-label">Topic</label>
                                        <input type="text" class="form-control" name="topic" value="{{ old('topic') }}" onclick="hideError(this)" />
                                        @if($errors->has('topic'))
                                            <div class="text-danger" id="topic">{{ $errors->first('topic') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="qustionentity" class="form-label">Question</label>
                                        <textarea class="form-control" name="question" id="qustionentity" rows="2" onclick="hideError(this)" >{{ old('topic') }}</textarea>
                                        @if($errors->has('question'))
                                            <div class="text-danger" id="question">{{ $errors->first('question') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="opt1" class="form-label">option 1</label>
                                        <textarea class="form-control"  name="option1" rows="1" onclick="hideError(this)" >{{ old('option1') }}</textarea>
                                        @if($errors->has('option1'))
                                            <div class="text-danger" id="option1">{{ $errors->first('option1') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="opt2" class="form-label">option 2</label>
                                        <textarea class="form-control" name="option2" rows="1" onclick="hideError(this)" >{{ old('option2') }}</textarea>
                                        @if($errors->has('option2'))
                                            <div class="text-danger" id="option2">{{ $errors->first('option2') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="opt3" class="form-label">option 3</label>
                                        <textarea class="form-control" name="option3" rows="1" onclick="hideError(this)" >{{ old('option3') }}</textarea>
                                        @if($errors->has('option3'))
                                            <div class="text-danger" id="option3">{{ $errors->first('option3') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="opt4" class="form-label">option 4</label>
                                        <textarea class="form-control" name="option4" rows="1" onclick="hideError(this)" >{{ old('option4') }}</textarea>
                                        @if($errors->has('option4'))
                                            <div class="text-danger" id="option4">{{ $errors->first('option4') }}</div>
                                        @endif
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Uploade Question</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection