@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section>
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                @if(Session::has('success'))
                                <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                                @endif
                                <h6>Add Gallery</h6>
                                <button class="btn btn-secondary addBtn"><a href="{{ route('view', 'gallery') }}">Gallery List</a></button>
                            </div>
                            <div class="card-body">
                                <form role="form" method="post" action="{{route('addGallery')}}" enctype="multipart/form-data">
                                    @csrf
                                    <label>Categories</label>
                                    <div class="mb-3">
                                        <select class="form-control" value="{{ old('categories') }}" name="categories" onclick="hideError(this)">
                                            <option>--Select--</option>
                                            <option>Company</option>
                                            <option>College</option>
                                            <option>Student</option>
                                            <option>Seminor</option>
                                        </select>
                                        @if($errors->has('categories'))
                                        <div class="text-danger" id="categories">{{ $errors->first('categories') }}</div>
                                        @endif
                                        <!-- <input type="text" class="form-control" placeholder="Name" value="{{ old('categories') }}" name="categories" onclick="hideError(this)" aria-label="Name" aria-describedby="email-addon">
                                        @if($errors->has('categories'))
                                        <div class="text-danger" id="categories">{{ $errors->first('categories') }}</div>
                                        @endif -->
                                    </div>
                                    <label>Image</label>
                                    <div class="mb-3">
                                        <input type="file" class="form-control" name="image" onclick="hideError(this)" aria-label="File" aria-describedby="password-addon">
                                        @if($errors->has('image'))
                                        <div class="text-danger" id="image">{{ $errors->first('image') }}</div>
                                        @endif
                                    </div>
                                    <label>Description</label>
                                    <div class="mb-3">
                                        <textarea class="form-control" name="description" onclick="hideError(this)">{{ old('description') }}</textarea>
                                        @if($errors->has('description'))
                                        <div class="text-danger" id="description">{{ $errors->first('description') }}</div>
                                        @endif
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection