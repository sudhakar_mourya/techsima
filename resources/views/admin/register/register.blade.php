@extends('layout.admin')
@section('admindata')
<div class="container-fluid py-4">
  <div class="row">
    <div class="col-12">
      <div class="card mb-4">
        <div class="card-header pb-0">
          @if(Session::has('success'))
            <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
          @endif
          <h6>Data Table</h6>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
          <div class="table-responsive p-0">
            <table class="table align-items-center mb-0">
              <thead>
                <tr>
                  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">#Id</th>
                  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Training Mode</th>
                  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Training Name</th>
                  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Technology</th>
                  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Education</th>
                  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Course Year</th>
                  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Father Name</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">Email</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">Mobile</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">Alternate Mobile</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">College Name</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">Payment Type</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">Payment Mode</th>
                  <th class="text-center text-uppercase text-secondary text-md font-weight-bolder opacity-7">Amount</th>
                  <th class="text-secondary opacity-7">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($records as $register)
                  <tr>
                    <td class="align-middle">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->id }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->training_mode }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->training_name }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->technology }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->education }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->course_year }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->father_name }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->email }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->mobile }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->alt_mobile }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->collage_name }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->payment_type }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->payment_mode }}</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-xs font-weight-bold mb-0">{{ $register->amount }}</p>
                    </td>
                    <td class="align-middle">
                      <a href="javascript:;" class="text-secondary font-weight-bold text-xs badge badge-sm bg-gradient-warning" data-toggle="tooltip" data-original-title="Edit user">
                        <form method="POST" action="{{ route('delete', ['register', $register->id]) }}">
                          @csrf
                          <input type="submit" class="d-sm-inline d-none" value="Delete">
                        </form>
                      </a>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection