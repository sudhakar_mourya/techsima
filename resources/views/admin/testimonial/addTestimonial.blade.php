@extends('layout.admin')
@section('admindata')
<main class="main-content  mt-0">
    <section class="bg-white">
        <div class="page-header min-vh-75">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8 col-md-12 d-flex flex-column mx-auto">
                        <div class="card-header pb-0 mt-8">
                            <div class="">
                                @if(Session::has('success'))
                                    <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                                @endif
                                <h6>Add Testimonial</h6>
                                <button class="btn btn-secondary addBtn"><a href="{{ route('view', 'testimonial') }}">Testimonial List</a></button>
                            </div>
                            <div class="card-body">
                                <form role="form" method="post" action="{{route('addTestimonial')}}" enctype="multipart/form-data">
                                    @csrf
                                    <label>Desination</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" name="desination" onclick="hideError(this)"  placeholder="Name" aria-label="Name" aria-describedby="name-addon">
                                        @if($errors->has('desination'))
                                            <div class="text-danger" id="desination">{{ $errors->first('desination') }}</div>
                                        @endif
                                    </div>
                                    <label>Name</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" name="name" onclick="hideError(this)"  placeholder="Name" aria-label="Email" aria-describedby="email-addon">
                                        @if($errors->has('name'))
                                            <div class="text-danger" id="name">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="FormControlTextarea1" class="form-label">Review</label>
                                        <textarea class="form-control" name="review" onclick="hideError(this)"  id="FormControlTextarea1" rows="3"></textarea>
                                        @if($errors->has('review'))
                                            <div class="text-danger" id="review">{{ $errors->first('review') }}</div>
                                        @endif
                                    </div>

                                    <div class="mb-3">
                                        <label for="formFileMultiple" class="form-label">Photo</label>
                                        <input class="form-control" name="image" onclick="hideError(this)"  type="file" id="formFileMultiple" multiple>
                                        @if($errors->has('image'))
                                            <div class="text-danger" id="image">{{ $errors->first('image') }}</div>
                                        @endif
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Send Data</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection