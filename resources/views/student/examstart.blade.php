@extends('layout.student')
<!-- @section('studentdata') -->


<section class="examheader">
    <div class="container">
        <div class="row">
            <div class="col-md-4 time-duration">
                <span>Time duration : 60 Mintes</span>
            </div>
            <div class="col-md-4 psl-detail">
                <span>Name - {{ Auth::user()->name }}</span>
            </div>
            <div class="col-md-4 time">
                <span>Time - <b id="time">59:59</b></span>
            </div>
        </div>
    </div>
</section>
<section class="exam-outer">
    <div class="container">
        <div class="row">
            @if(Session::has('success'))
                <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
            @endif
            <div class="col-md-4 question-list-outer">
                <div class="question-list mt-5">
                @foreach($records as $question)
                    <span onclick="gotopage({{ $loop->iteration }})"> {{ $loop->iteration }} </span>
                @endforeach                  
                </div>
                <div class="qst-atm mt-4">
                    <ul>
                        <li><span class="count-number" id="remain">{{ count($records) }}</span><span class="qust-ttl ps-4 pt-1">Not Attempt Question</span></li>
                        <li><span class="count-number" id="attemp">0</span><span class="qust-ttl ps-4 pt-1">Attempt Answer</span></li>
                        <li><span class="count-number">{{ count($records) }}</span><span class="qust-ttl ps-4 pt-1">Total Question</span></li>
                    </ul>               
                </div>                
            </div>
            <div class="col-md-8">
                <form id="questionForm" method="post" action="{{ route('examStart', [$topic]) }}">
                @csrf
                @foreach($records as $question)
                <div class="quest-count question-answer d-none" id="{{ 'question'.$loop->iteration }}">
                    <div class="mt-5">
                        <span> Question {{ $loop->iteration }} : </span>               
                        <p>{{ $question->question }}</p>
                        <span> Answer : </span>
                        @foreach($question->option as $item)
                        <p><input type="radio" name="{{$question->id}}" value="{{$item->id}}" onclick="attempQuestion({{ count($records) }})"><span>{{ $item->answer }}</span></p>
                        @endforeach
                    </div> 

                    <div class="mt-5">
                        <nav aria-label="Page navigation">
                            <ul class="pagination">
                                <li class="page-item ms-2" onclick="gotopage({{ $loop->iteration - 1 }})"><button type="button" class="page-link"  {{ $loop->iteration == 1 ? 'disabled' : '' }}>Previous Question</button></li>
                                <li class="page-item" onclick="gotopage({{ $loop->iteration + 1 }})">
                                <button type="button" class="page-link" tabindex="-1" aria-disabled="true"  {{ $loop->iteration == count($records) ? 'disabled' : '' }}>Next question</button>
                                </li>

                                <li class="page-item ms-2"><a class="page-link" href="javascript:void(0);" onclick="document.getElementById('questionForm').submit()">Final Submit</a></li>
                            </ul>
                        </nav>
                    </div>      
                </div>
                @endforeach 
                </form>  
            </div>            
        </div>
    </div>
</section>

<script>
    document.getElementById('question1').classList.remove('d-none')
    function gotopage(id){
        document.querySelectorAll(".quest-count").forEach(function (ele){ ele.classList.add('d-none') })
        document.getElementById('question'+ id).classList.remove('d-none')
    }


    function attempQuestion(total){
        let checked_question = document.querySelectorAll('input[type = "radio"]:checked');
        document.getElementById('attemp').innerText = checked_question.length
        document.getElementById('remain').innerText = total - checked_question.length
    }
    
    var min = 59;
    var sec = 59
    setInterval(() => {
        sec = sec -1
        if(sec == 0){
            min = min -1
            sec = 59
        }
        if(min == 0 && sec == 0){
            document.getElementById('questionForm').submit()
        }
        let time = document.getElementById('time')
        time.innerText = min + ':' + sec
    }, 1000);

</script>

<!-- @endsection -->

