@extends('layout.student')
@section('studentdata')
<section class="dashboard-section">
   <h1 class="text-center text-uppercase p-5 fw-bold">Dashboard</h1>
   <div class="container">
      <div class="row">
         <div class="col-md-9 dashboard-product">
            <div class="row">
               <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 product-circle-outer">
                  <a href="">
                     <div class="product-circle">
                        <ul>
                           <li><i class="fas fa-laptop icon-d" aria-hidden="true"></i> </li>
                           <li><span class="title">Study Center</span></li>
                        </ul>
                     </div>
                  </a>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 product-circle-outer">
                  <a href="">
                     <div class="product-circle">
                        <ul>
                           <li> <i class="fas fa-code icon-d" aria-hidden="true"></i></li>
                           <li><span class="title">Video & Code Center</span></li>
                        </ul>
                     </div>
                  </a>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 product-circle-outer">
                  <a href="/my-profile">
                     <div class="product-circle">
                        <ul>
                           <li><i class="fas fa-user icon-d" aria-hidden="true"></i> </li>
                           <li><span class="title">Update Profile</span></li>
                        </ul>
                     </div>
                  </a>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 product-circle-outer">
                  <a href="/exam">
                     <div class="product-circle">
                        <ul>
                           <li><i class="fas fa-pencil-square-o icon-d" aria-hidden="true"></i></li>
                           <li><span class="title">Online Test</span></li>
                        </ul>
                     </div>
                  </a>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 product-circle-outer">
                  <a href="">
                     <div class="product-circle">
                        <ul>
                           <li><i class="fas fa-video-camera icon-d" aria-hidden="true"></i> </li>
                           <li><span class="title">Live Meeting</span></li>
                        </ul>
                     </div>
                  </a>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 product-circle-outer">
                  <a href="">
                     <div class="product-circle">
                        <ul>
                           <li><i class="fas fa-key icon-d" aria-hidden="true"></i> </li>
                           <li><span class="title">Change Password</span></li>
                        </ul>
                     </div>
                  </a>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 product-circle-outer">
                  <a href="">
                     <div class="product-circle">
                        <ul>
                           <li><i class="fas fa-inr icon-d" aria-hidden="true"></i> </li>
                           <li><span class="title">Fee Payment</span></li>
                        </ul>
                     </div>
                  </a>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 product-circle-outer">
                  <a href="">
                     <div class="product-circle">
                        <ul>
                           <li><i class="fas fa-question-circle icon-d" aria-hidden="true"></i> </li>
                           <li><span>Help</span></li>
                        </ul>
                     </div>
                  </a>
               </div>
               <!-- testimonial -->
               <section class="studentFeedback pt-3 pb-5">
                  <div class="container">
                     <div class="row text-center">
                        <div class="col-12">
                           <h1 class="fw-bold text-white pt-3">Most valuable feedback </h1>
                           <hr class="bg-white mb-4 mt-0 d-inline-block mx-auto" style="width: 100px; height:3px;">
                           <p class="p-text text-white">What our student's are saying</p>
                        </div>
                     </div>
                     <div class="row align-items-md-center text-white">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                           <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                              <!-- Wrapper for slides -->
                              <div class="carousel-inner">
                                 <div class="carousel-item active">
                                    <div class="row p-4">
                                       <div class="t-card">
                                          <i class="fa fa-quote-left" aria-hidden="true"></i>
                                          <p class="lh-lg">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum facere similique at quo asperiores illum praesentium assumenda explicabo dolorum perspiciatis soluta eveniet distinctio, illo amet provident exercitationem unde ducimus necessitatibus.
                                          </p>
                                          <i class="fa fa-quote-right" aria-hidden="true"></i><br>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-2 pt-3">
                                             <img src="https://source.unsplash.com/300x300/?girl" class="rounded-circle img-responsive img-fluid">
                                          </div>
                                          <div class="col-sm-10">
                                             <div class="arrow-down d-none d-lg-block"></div>
                                             <h4><strong>sudhakar</strong></h4>
                                             <p class="testimonial_subtitle"><span>Associate Software Engineer</span><br>
                                                <span>Code</span>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="carousel-item">
                                    <div class="row p-4">
                                       <div class="t-card">
                                          <i class="fa fa-quote-left" aria-hidden="true"></i>
                                          <p class="lh-lg">Lorem Ipsum ist ein einfacher
                                             Lorem Ipsum ist in der Industrie bereits der
                                             perspiciatis unde omnis iste natus error sit
                                             laudantium, totam rem aperiam, eaque ipsa quae
                                             architecto beatae vitae dicta sunt
                                          </p>
                                          <i class="fa fa-quote-right" aria-hidden="true"></i><br>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-2 pt-4">
                                             <img src="https://source.unsplash.com/300x300/?man" class="rounded-circle img-responsive img-fluid">
                                          </div>
                                          <div class="col-sm-10">
                                             <div class="arrow-down d-none d-lg-block"></div>
                                             <h4><strong>xyz</strong></h4>
                                             <p class="testimonial_subtitle"><span>
                                                   digital strategist</span><br>
                                                <span> digital</span>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="carousel-item">
                                    <div class="row p-4">
                                       <div class="t-card">
                                          <i class="fa fa-quote-left" aria-hidden="true"></i>
                                          <p class="lh-lg">Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie.
                                             Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text "Sed ut .
                                          </p>
                                          <i class="fa fa-quote-right" aria-hidden="true"></i><br>
                                       </div>
                                       <div class="row text-lg-start">
                                          <div class="col-sm-2 pt-4 align-items-center">
                                             <img src="https://source.unsplash.com/300x300/?businessman" class="rounded-circle img-responsive img-fluid">
                                          </div>
                                          <div class="col-sm-10">
                                             <div class="arrow-down d-none d-md-block"></div>
                                             <h4><strong>XYZ</strong></h4>
                                             <p class="testimonial_subtitle"><span>Web Technologist</span><br>
                                                <span>digital</span>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="controls push-right">
                              <a class="left fa fa-chevron-left text-white st-btn st-btn btn-outline-light btn-left" href="#carouselExampleCaptions" data-bs-slide="prev"></a>
                              <a class="right fa fa-chevron-right text-white st-btn st-btn btn-outline-light btn-left" href="#carouselExampleCaptions" data-bs-slide="next"></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <!-- testimonial -->
            </div>
         </div>
         <div class="col-md-3">
            <div class="profile-show bg-white mt-4 mb-4 pt-4 pb-3">
               <div class="show-pic">
                  <img src="{{asset('/assets/img/vocational.png') }}" class="img-fluid p-3">
               </div>
               <p class="showName text-center mb-0 pt-2">Name</p>
               <p class="text-center mb-0">Branch - Year</p>
               <p class="profileComplete text-center">Profile Complete: 80%</p>
               <p class="text-center">
                  <a class="pintrest-icon" href="javascript:void(0);" data-toggle="tooltip" title="Pinterest"><i class="fab fa-pinterest-p"></i></a>
                  <a class="facebook-icon" href="javascript:void(0);" data-toggle="tooltip" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                  <a class="twitter-icon" href="javascript:void(0);" data-toggle="tooltip" title="Twitter"><i class="fab fa-instagram"></i></a>
                  <a class="linkedin-icon" href="javascript:void(0);" data-toggle="tooltip" title="Linkedin"><i class="fab fa-linkedin-in"></i></a>
                  <a class="youtube-icon" href="javascript:void(0);" data-toggle="tooltip" title="Youtube"><i class="fab fa-youtube"></i></a>
               </p>
               <p class="text-center">
                  <a href="#">Go Profile</a>
               </p>
            </div>
            <div class="profile-show bg-white mt-4 mb-4 pt-4 pb-3">
               <p class="showName text-center mb-0 pt-2">Stream Live Class </p>
               <div class="show-pic d-flex justify-content-center align-items-center">
                  <i class="fas fa-video fs-1"></i>
               </div>
               <p class="text-center mb-0">Class Timing: 5pm</p>
               <p class="profileComplete text-center">Forntend classes</p>
               <a href="">
                  <p class="text-center">Go Live classes Timing</p>
               </a>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection