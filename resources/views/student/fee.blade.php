@extends('layout.student')
@section('studentdata')
<section class="pt-5">
    <h1 class="pt-5 text-center">Fee Payment Method</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-6 fee-image">
                <img src="{{asset('assets/img/fee.jpg') }}" alt="fee-image" class="img-fluid">
            </div>
            <div class="col-md-6 fee-Method p-5">
                <h3>Pyment Now</h3>
                <hr class="w-75" />
                <p><span><img src="{{asset('assets/img/phonepay.svg')}}" class="img-fluid pay-logo"></span><span class="ps-5">8090995797</span></p>
                <hr class="w-75" />
                <p><span><img src="{{asset('assets/img/googlepay.svg')}}" class="img-fluid pay-logo"></span><span class="ps-5">8090995797</span></p>
                <hr class="w-75" />
                <p><span><img src="{{asset('assets/img/paytym.svg')}}" class="img-fluid pay-logo"></span><span class="ps-5">8090995797</span></p>
                <hr class="w-75" />
            </div>
        </div>
    </div>
</section>
@endsection