@extends('layout.student')
@section('studentdata')
<section class="exam-section pt-5">
    <h1 class="text-center text-white text-uppercase pt-5">Welcome To Techsima Online Exam Portal</h1>
    <div class="container-fluid">
        <div class="row">
            <div class="exam-image pt-5">
                <h6 class="text-center text-white"> Click here start exam now</h6>
                <div class="next-page-link d-flex justify-content-center pt-5">
                    <a href="{{route('examStart', ['html'])}}">Start Exam</a>
                    <a class="ms-3" href="">Start Quiz</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection