@extends('layout.student')
@section('studentdata')
 <div class="main-heading">
     <h2 class="text-uppercase text-center p-5">Update Student Profile</h2>
</div>
<section class="update-section">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <form role="form" method="post" action="{{route('updateProfile')}}" enctype="multipart/form-data">
                 <div class="col-md-8 m-auto update-outer">
                    @if(Session::has('success'))
                        <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                    @endif
                    <div class="profile-photo">
                        <img src="{{ isset($profile->image) ? env('FILE_URL').$profile->image : '' }}" class="img-fluid">
                    </div>
                     <div class="row">
                        @csrf
                        <div class="col-md-6 mb-3">
                            <label for="fname" class="form-label">Name</label>
                            <input type="text" name="name" class="form-control"  value="@if($profile->name) {{$profile->name}} @else {{ old('name') }} @endif" name="name" onclick="hideError(this)" />
                            @if($errors->has('name'))
                                <div class="text-danger" id="name">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="father_name" class="form-label">Father's Name</label>
                            <input type="text" name="father_name" class="form-control" value="@if($profile->father_name) {{$profile->father_name}} @else {{ old('father_name') }} @endif" onclick="hideError(this)" />
                            @if($errors->has('father_name'))
                                <div class="text-danger" id="father_name">{{ $errors->first('father_name') }}</div>
                            @endif
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="phone" class="form-label">Mobile No</label>
                            <input type="text" name="phone" class="form-control" value="@if($profile->phone) {{$profile->phone}} @else {{ old('phone') }} @endif " onclick="hideError(this)" />
                            @if($errors->has('phone'))
                                <div class="text-danger" id="phone">{{ $errors->first('phone') }}</div>
                            @endif
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="email" class="form-label">Gmail Id</label>
                            <input type="email" name="email" class="form-control" disabled value="@if($profile->email) {{$profile->email}} @else {{ old('email') }} @endif" />
                        </div>
                        <div class="col-md-6 mb-3">
							<label for="high_quallification" class="form-label">High Eduction</label>
							<select class="form-select form-select-md" aria-label=".form-select-md" name="high_quallification" onclick="hideError(this)" >
								<option value="">Please Select Education Name</option>
								<option selected="@if($profile->high_quallification == 'B.TECH(IT)' || old('high_quallification') == 'B.TECH(IT)') true  @else false @endif" value="B.TECH(IT)">B.TECH(IT)</option>
								<option selected="@if($profile->high_quallification == 'B.TECH(CS)' || old('high_quallification') == 'B.TECH(CS)') true  @else false @endif" value="B.TECH(CS)">B.TECH(CS)</option>
								<option selected="@if($profile->high_quallification == 'MCA' || old('high_quallification') == 'MCA') true  @else false @endif" value="MCA">MCA</option>
								<option selected="@if($profile->high_quallification == 'BCA' || old('high_quallification') == 'BCA') true  @else false @endif" value="BCA">BCA</option>
								<option selected="@if($profile->high_quallification == 'DIPLOMA(IT)' || old('high_quallification') == 'DIPLOMA(IT)') true  @else false @endif" value="DIPLOMA(IT)">DIPLOMA(IT)</option>
								<option selected="@if($profile->high_quallification == 'DIPLOMA(CS)' || old('high_quallification') == 'DIPLOMA(CS)') true  @else false @endif" value="DIPLOMA(CS)">DIPLOMA(CS)</option>
								<option selected="@if($profile->high_quallification == 'PGDCA' || old('high_quallification') == 'PGDCA') true  @else false @endif" value="PGDCA">PGDCA</option>
								<option selected="@if($profile->high_quallification == 'OTHER' || old('high_quallification') == 'OTHER') true  @else false @endif" value="OTHER">OTHER</option>
							</select>
                            @if($errors->has('high_quallification'))
                                <div class="text-danger" id="high_quallification">{{ $errors->first('high_quallification') }}</div>
                            @endif
                        </div>
                        <div class="col-md-6 mb-3">
							<label for="upload" class="form-label">Profile update</label>
							<input type="file" name="image" class="form-control">
                            @if($errors->has('image'))
                                <div class="text-danger" id="image">{{ $errors->first('image') }}</div>
                            @endif
                        </div>
                        <div class="col-md-12 mb-3">
							<label for="address" class="form-label">Address</label>
							<textarea name="address" class="form-control" onclick="hideError(this)">@if($profile->address) {{$profile->address}} @else {{ old('address') }} @endif</textarea>
                            @if($errors->has('address'))
                                <div class="text-danger" id="address">{{ $errors->first('address') }}</div>
                            @endif
                        </div>
                        <div class="col-md-6 mb-3"><br>
                            <input type="submit" value="UPDATE PROFILE" class="btn mt-2 pt-1 pb-1 ps-2 pe-2">
                        </div>
                    </div>
                 </div>             
            </form>
            <div class="col-sm-2"></div>
        </div>
    </div>
</section>
@endsection