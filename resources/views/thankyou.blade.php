@extends('layout.home')
@section('frontdata')
<section class="error-page">
    <div class="container-fluid">
        <div class="row">
            <div class="error-page-inner thankyou-page">
                <i class='fas fa-smile text-white pb-4' style="font-size:5rem"></i>
                <h1 class="pb-4">Thank You!</h1>

                <h2>you are given PRECIOUS TIME TO ME.</h2>
                <a class="btn-error" href="{{ route('home') }}">GO BACK TO HOME</a>
            </div>
        </div>
    </div>
</section>
@endsection