@extends('layout.home')
@section('frontdata')
<!-- banner section start -->
<section class="bg-white bg-banner">
	<div class="container-fluid">
		<div class="row">
			<!-- <div class="col-lg-6 background-slider">
				<div class="row background-slider-outer">				
                    <div class="breadcrumb-section d-flex justify-content-center align-items-center">
                        <nav aria-label="breadcrumb ">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Library</li>
                            </ol>
                        </nav>
                    </div>
				</div>
			</div> -->
			<div class="slider">
				<img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
			</div>
		</div>
	</div>
</section>
<!-- banner section end -->
<section class="registerUs ti_padding_top_60">
	<div class="container">
		<div class="row">
			<div class="col-md-12 registration p-5">
				@if(Session::has('success'))
				<h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
				@endif
				<h1 class="form-title">Register Here</h1>
				<form method="post" action="{{route('register')}}">
					@csrf
					<div class="row">
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Do you wnat to ?</label>
							<select class="form-select form-select-md form-control" aria-label=".form-select-md" name="training_mode" onclick="hideError(this)">
								<option selected="">Traning mode</option>
								<option value="online_traning">Online Traning</option>
								<option value="offline_traning">Offline Traning</option>
							</select>
							@if($errors->has('training_mode'))
							<div class="text-danger" id="training_mode">{{ $errors->first('training_mode') }}</div>
							@endif
						</div>
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Technology</label>
							<select class="form-select form-select-md form-control" aria-label=".form-select-md" name="technology" onclick="hideError(this)">
								<option selected>Please Select Course</option>
								<option value="PHP">PHP</option>
								<option value="JAVA">JAVA</option>
								<option value="ANDROID">ANDROID</option>
								<option value="PYTHON">PYTHON</option>
								<option value=".NET">.NET</option>
							</select>
							@if($errors->has('technology'))
							<div class="text-danger" id="technology">{{ $errors->first('technology') }}</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Training Name</label>
							<select class="form-select form-select-md form-control" aria-label=".form-select-md" name="training_name" onclick="hideError(this)">
								<option selected>Please Select Training Name</option>
								<option value="summer_traning">Summer Traning</option>
								<option value="winter_traning">Winter Traning</option>
								<option value="industrial_traning">Industrial Traning</option>
								<option value="internship_traning">Internship Traning</option>
							</select>
							@if($errors->has('training_name'))
							<div class="text-danger" id="training_name">{{ $errors->first('training_name') }}</div>
							@endif
						</div>
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Eduction</label>
							<select class="form-select form-select-md form-control" aria-label=".form-select-md" name="education" onclick="hideError(this)">
								<option selected>Please Select Education Name</option>
								<option value="B.TECH(IT)">B.TECH(IT)</option>
								<option value="B.TECH(CS)">B.TECH(CS)</option>
								<option value="MCA">MCA</option>
								<option value="BCA">BCA</option>
								<option value="DIPLOMA(IT)">DIPLOMA(IT)</option>
								<option value="DIPLOMA(CS)">DIPLOMA(CS)</option>
								<option value="PGDCA">PGDCA</option>
								<option value="OTHER">OTHER</option>
							</select>
							@if($errors->has('education'))
							<div class="text-danger" id="education">{{ $errors->first('education') }}</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Course Year</label>
							<select class="form-select form-select-md form-control" aria-label=".form-select-md" name="course_year" onclick="hideError(this)">
								<option selected>Please Course Select Year</option>
								<option value="first_year">First Year</option>
								<option value="second_year">Second Year</option>
								<option value="third_year">Third Year</option>
								<option value="forth_year">Forth Year</option>
								<option value="Completed">Completed</option>
							</select>
							@if($errors->has('course_year'))
							<div class="text-danger" id="course_year">{{ $errors->first('course_year') }}</div>
							@endif
						</div>
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Name</label>
							<input type="text" class="form-control" name="name" onclick="hideError(this)">
							@if($errors->has('name'))
							<div class="text-danger" id="name">{{ $errors->first('name') }}</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Father's Name</label>
							<input type="text" class="form-control" name="father_name" onclick="hideError(this)">
							@if($errors->has('father_name'))
							<div class="text-danger" id="father_name">{{ $errors->first('father_name') }}</div>
							@endif
						</div>
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Email address</label>
							<input type="text" class="form-control" name="email" onclick="hideError(this)">
							@if($errors->has('email'))
							<div class="text-danger" id="email">{{ $errors->first('email') }}</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Mobile Number</label>
							<input type="text" class="form-control" name="mobile" onclick="hideError(this)">
							@if($errors->has('mobile'))
							<div class="text-danger" id="mobile">{{ $errors->first('mobile') }}</div>
							@endif
						</div>
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Alternate Mobile Number</label>
							<input type="text" class="form-control" aria-describedby="alt_mobileHelp" name="alt_mobile" onclick="hideError(this)">
							@if($errors->has('alt_mobile'))
							<div class="text-danger" id="alt_mobile">{{ $errors->first('alt_mobile') }}</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">College Name</label>
							<input type="text" class="form-control" name="college_name" onclick="hideError(this)">
							@if($errors->has('college_name'))
							<div class="text-danger" id="college_name">{{ $errors->first('college_name') }}</div>
							@endif
						</div>
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Payment Type</label>
							<select class="form-select form-select-md form-control" aria-label=".form-select-md" id="payment_type" name="payment_type">
								<option selected>Please Select Paymant Type</option>
								<option value="registration_fee">Registration Fee</option>
								<option value="full_fee">Full Fee</option>
							</select>
							@if($errors->has('payment_type'))
							<div class="text-danger" id="payment_type">{{ $errors->first('payment_type') }}</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Payment mode</label>
							<select class="form-select form-select-md form-control" aria-label=".form-select-md" name="payment_mode" onclick="hideError(this)">
								<option selected>Please Select Paymant mode</option>
								<option value="online">Online</option>
								<option value="cash">Cash</option>
							</select>
							@if($errors->has('payment_mode'))
							<div class="text-danger" id="payment_mode">{{ $errors->first('payment_mode') }}</div>
							@endif
						</div>
						<div class="col-md-6 mb-3">
							<label for="Text" class="form-label">Amount</label>
							<input type="text" class="form-control" id="amount" readOnly name="amount">
						</div>
					</div>
					<div class="pt-4">
						<button type="submit" class="btn-training">Submit</button>
						<button type="reset" class="btn-training">Reset</button>
					</div>

				</form>
			</div>
		</div>
	</div>
</section>
@endsection