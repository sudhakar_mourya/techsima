<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	@php
	$title = (explode('/', request()->url()))[count((explode('/', request()->url()))) - 1];
	@endphp
	<title>TECHSIMA || {{ $title == 'localhost:7000' || $title == 'localhost:8000' ? 'Home' : strtoupper($title) }}</title>
	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link rel="shortcut icon" href="{{asset('assets/img/favicon.png') }}" type="image/x-icon">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" crossorigin="anonymous" />
	<!-- Styles -->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/app.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
	<link type="text/css" rel="stylesheet" href="{{asset('assets/css/app.css.map')}}">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- Option 1: Bootstrap Bundle with Popper -->
</head>

<body onload="flashmsg()">
	<div id="preloader">
		<div class="center">
			<div class="ring"></div>
			<span>Loading...</span>
		</div>
	</div>
	<div id="topHead">
		<div class="container-fluid topHeader">
			<div class="row">
				<div class="col-md-4 col-sm-12 socialIcon">
					<a class="pintrest-icon" href="javascript:void(0);" data-toggle="tooltip" title="Pinterest"><i class="fab fa-pinterest-p"></i></a>
					<a class="facebook-icon" href="javascript:void(0);" data-toggle="tooltip" title="Facebook"><i class="fab fa-facebook-f"></i></a>
					<a class="twitter-icon" href="javascript:void(0);" data-toggle="tooltip" title="Twitter"><i class="fab fa-twitter"></i></a>
					<a class="linkedin-icon" href="javascript:void(0);" data-toggle="tooltip" title="Linkedin"><i class="fab fa-linkedin-in"></i></a>
					<a class="youtube-icon" href="javascript:void(0);" data-toggle="tooltip" title="Youtube"><i class="fab fa-youtube"></i></a>
				</div>
				<div class="col-md-4 col-sm-12 socialIcon contact-info-mail">
					<a href="mailto:info@techsima.com"><i class="far fa-envelope pe-2"></i>info@techsima.com</a>
				</div>
				<div class="col-md-4 col-sm-12 socialIcon contact-info">
					<a href="tel:+91 7800050976"><i class="fas fa-mobile-alt pe-2"></i>+91 - 7800050976</a>
				</div>
			</div>
		</div>
	</div>
	<header class="primary-header">
		<div class="logo ms-4">
			<img src="{{asset('/assets/img/logo.svg') }}" alt="Company Logo" class="img-fluid">
		</div>
		<button aria-control="primary-navigation" aria-expanded="false" class="mobile-nav-toggle">
			<span class="sr-only">Menu</span>
		</button>
		<nav class="me-4">
			<ul data-visible="false" id="primary-navigation" class="primary-navigation">
				<li>
					<a href="{{ route('home') }}">Home</a>
				</li>
				<li>
					<a href="{{ route('aboutus') }}">About Us</a>
				</li>
				<li>
					<a href="{{ route('traning') }}">Traning</a>
				</li>
				<li>
					<a href="{{ route('registration') }}">Registration</a>
				</li>
				<li>
					<a href="{{ route('contactus') }}">Contact Us</a>
				</li>
				<li>
					<a href="{{ route('faq') }}">Faq</a>
				</li>
				<li>
					<a href="{{ route('courses') }}">Courses</a>
				</li>
				<li>
					<a href="{{ route('services') }}">Services</a>
				</li>
				<li>
					<a href="{{ route('gallary') }}">Gallary</a>
				</li>
			</ul>
		</nav>
	</header>