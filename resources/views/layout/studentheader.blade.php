<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	@php
	$title = (explode('/', request()->url()))[count((explode('/', request()->url()))) - 1];
	@endphp
	<title>TECHSIMA || {{ $title == 'localhost:7000' || $title == 'localhost:8000' ? 'Home' : strtoupper($title) }}</title>
	<!-- Fonts -->
	<link rel="shortcut icon" href="{{asset('assets/img/favicon.png') }}" type="image/x-icon">
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Styles -->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/app.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/student.techsima.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
	<link type="text/css" rel="stylesheet" href="{{asset('assets/css/app.css.map')}}">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
</head>

<body class="bg-white">
	<header class="header" id="studentHeader">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3 company-logo">
					<div class="logo ms-4">
						<img src="{{asset('/assets/img/logo.svg') }}" alt="Company Logo" class="img-fluid">
					</div>
				</div>
				<div class="col-sm-6 student-id">
					<p class="std-id">
						{{ Auth::user()->name }} : Free Course
					</p>
				</div>
				<div class="col-sm-3 noti-profile">
					<div class="noti-icon">
						<i class="fas fa-bell">
							<span>1</span>
						</i>
					</div>
					<div class="image-show-profile p-1">
						<img src="{{asset('/assets/img/vocational.png') }}" class="img-fluid" alt="pp">
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="navigation">
		<ul>
			<li class="navigation-list">
				<a href="/dashboard">
					<span class="icon"><i class="fas fa-home" aria-hidden="true"></i></span>
					<span class="title"> Home</span>
				</a>
			</li>
			<li class="navigation-list">
				<a href="#">
					<span class="icon"><i class="fas fa-laptop" aria-hidden="true"></i></span>
					<span class="title">Study Center</span>
				</a>
			</li>
			<li class="navigation-list">
              <a href="{{ route('change-password') }}" >
				<span class="icon"><i class="fas fa-laptop" aria-hidden="true"></i></span>
			  	<span class="title">Change Password</span>
              </a>
            </li>
			<li class="navigation-list">
				<a href="#">
					<span class="icon"><i class="fas fa-code" aria-hidden="true"></i></span>
					<span class="title">Video & Code Center</span>
				</a>
			</li>
			<li class="navigation-list">
				<a href="/my-profile">
					<span class="icon"><i class="fas fa-user" aria-hidden="true"></i></span>
					<span class="title">Update Profile</span>
				</a>
			</li>
			<li class="navigation-list">
				<a href="/exam">
					<span class="icon"><i class="fas fa-pencil-square-o" aria-hidden="true"></i></span>
					<span class="title">Online Test</span>
				</a>
			</li>
			<li class="navigation-list">
				<a href="#">
					<span class="icon"><i class="fas fa-video-camera" aria-hidden="true"></i></span>
					<span class="title">Live Meeting</span>
				</a>
			</li>
			<li class="navigation-list">
				<a href="#">
					<span class="icon"><i class="fas fa-key" aria-hidden="true"></i></span>
					<span class="title">Change PassWord</span>
				</a>
			</li>
			<li class="navigation-list">
				<a href="#">
					<span class="icon"><i class="fas fa-inr" aria-hidden="true"></i> </span>
					<span class="title">Fee Payment</span>
				</a>
			</li>
			<li class="navigation-list">
				<a href="#">
					<span class="icon"><i class="fas fa-question-circle" aria-hidden="true"></i></span>
					<span class="title">Help</span>
				</a>
			</li>
			<li class="navigation-list">
              <a href="javascript:;" class="nav-link text-body font-weight-bold px-0">
                <form method="POST" action="{{ route('logout') }}">
                  @csrf
                  <i class="fa fa-user me-sm-1"></i>
                  <input type="submit" class="d-sm-inline d-none" value="Logout">
                </form>
              </a>
            </li>
		</ul>
	</div>
	<div class="toggle" onclick="toggleMenu()"></div>
	<script type="text/javascript">
		function toggleMenu() {
			let navigation = document.querySelector('.navigation');
			let toggle = document.querySelector('.toggle');
			navigation.classList.toggle('active');
			toggle.classList.toggle('active');
		}
	</script>