<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-6 col-sm-12 footerLogo">
				<div class="header-logo">
					<img src="{{asset('/assets/img/logo-footer.png') }}" alt="Company Logo" class="img-fluid">
				</div>
				<p class="text-white">Footer LogoTraining Institute is an outstanding HTML template targeting educational institutions, helping them establish strong identity on the internet without any real developing knowledge.</p>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12 connectWith">
				<h2 class="widget-title position-relative">Connect With Us</h2>
				<ul class="icon pt-4 pb-3">
					<li><a href="javascript:void(0);"><i class="fab fa-youtube"></i>YouTube</a></li>
					<li><a href="javascript:void(0);"><i class="fab fa-facebook-f"></i>Facebook</a></li>
					<li><a href="javascript:void(0);"><i class="fab fa-instagram"></i>Instagram</a></li>
					<li><a href="javascript:void(0);"><i class="fab fa-pinterest-p"></i>Pinterest</a></li>
					<li><a href="javascript:void(0);"><i class="fab fa-linkedin-in"></i>Linkedin</a></li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12 quickLink">
				<h2 class="widget-title position-relative">Quick Links</h2>
				<ul class="quick pt-4 pb-3">
					<li><a href="javascript:void(0);"><i class="fas fa-chevron-right"></i>Study Center</a></li>
					<li><a href="javascript:void(0);"><i class="fas fa-chevron-right"></i>Video and code center</a></li>
					<li><a href="javascript:void(0);"><i class="fas fa-chevron-right"></i>Online Test</a></li>
					<li><a href="javascript:void(0);"><i class="fas fa-chevron-right"></i>Help</a></li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12 findUs">
				<h2 class="widget-title position-relative">Find Us</h2>
				<ul class="info pt-4 pb-3">
					<li class="d-flex">
						<span><i class="fas fa-location-arrow"></i></span>
						<span class="ps-2">In front of, Rajarshi Dashrath Rajkiya Medical College, kushmaha, Faizabad Rd, Ayodhya, Uttar Pradesh 224135. </span>
					</li>
					<li>
						<a href="mailto:info@gmail.com"><i class="fas fa-envelope"></i>info@techsima.com</a>
					</li>
					<li>
						<a href="tel:+91 7800050976"><i class="fas fa-mobile-alt"></i>+91 7800050976</a>
					</li>
				</ul>
			</div>
			<hr class="bg-white" />
			<p class="text-white text-center">© Copyright 2022, All Rights Reserved <a class="text-white" href="#">Techsima</a></p>
		</div>
	</div>
</footer>


<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{asset('js/helper.js')}}"></script>
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
	AOS.init({
		offset: 120,
		delay: 0,
		easing: 'ease',
		duration: 400,
		disable: false,
		once: false,
		startEvent: 'DOMContentLoaded',
		throttleDelay: 99,
		debounceDelay: 50,
		disableMutationObserver: false,
	});
	//    function a() {
	// 				document.getElementById("navigate").style.width = "250px";
	// 				// var toggle=document.getElementById('navigate');
	// 				// toggle.classList.add('active');
	// 			}
</script>
<script type="text/javascript" src="{{asset('js/stdcustom.js')}}"></script>
</body>

</html>