<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/apple-icon.png')}}">
  <link rel="shortcut icon" href="{{asset('assets/img/favicon.png') }}" type="image/x-icon">
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" crossorigin="anonymous" />
  <!-- Nucleo Icons -->
  <link href="{{asset('assets/css/techsima-icons.css')}}" rel="stylesheet" />
  <link href="{{asset('assets/css/techsima-svg.css')}}" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- CSS Files -->
  <link id="pagestyle" href="{{asset('assets/css/techsima-dashboard.css?v=1.0.3')}}" rel="stylesheet" />
</head>

<body class="g-sidenav-show  bg-gray-100" onload="flashmsg()">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 " id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="/" target="_blank">
        <img src="{{asset('assets/img/logo.svg')}}" class="img-fluid" alt="main_logo" >
      </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100 h-100" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link  active" href="../pages/dashboard.html">
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'poster') }}">
            <span class="nav-link-text ms-1">Poster</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'contact') }}">
            <span class="nav-link-text ms-1">Contact</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'user') }}">
            <span class="nav-link-text ms-1">User</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'subscribe') }}">
            <span class="nav-link-text ms-1">Subscribers</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'faq') }}">
            <span class="nav-link-text ms-1">Faq</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'blog') }}">
            <span class="nav-link-text ms-1">Blog</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'testimonial') }}">
            <span class="nav-link-text ms-1">Testimonial</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'course') }}">
            <span class="nav-link-text ms-1">Course</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'enquiry') }}">
            <span class="nav-link-text ms-1">Enquiry</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'gallery') }}">
            <span class="nav-link-text ms-1">Gallery</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'register') }}">
            <span class="nav-link-text ms-1">Register</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'training') }}">
            <span class="nav-link-text ms-1">Training</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('view', 'service') }}">
            <span class="nav-link-text ms-1">Service</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('questionList') }}">
            <span class="nav-link-text ms-1">Question</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('answerList') }}">
            <span class="nav-link-text ms-1">Answer</span>
          </a>
        </li>
      </ul>
    </div>
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <h6 class="font-weight-bolder mb-0">Dashboard</h6>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-flex align-items-center">
              <a href="{{ route('change-password') }}" role="button" class="border nav-link text-body font-weight-bold px-0">
              Change Password
              </a>
            </li>
            <li class="nav-item d-flex align-items-center ms-2">
              <a href="javascript:;" class="nav-link text-body font-weight-bold px-0">
                <form method="POST" action="{{ route('logout') }}">
                  @csrf
                  <input type="submit" class="d-sm-inline d-none" value="Logout">
                </form>
              </a>
            </li>
            <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item px-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0">
                <i class="fa fa-cog fixed-plugin-button-nav cursor-pointer"></i>
              </a>
            </li>
            <li class="nav-item dropdown pe-2 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-bell cursor-pointer"></i>
              </a>
              <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4" aria-labelledby="dropdownMenuButton">
                <li class="mb-2">
                  <a class="dropdown-item border-radius-md" href="javascript:;">
                    <div class="d-flex py-1">
                      <div class="my-auto">
                        <img src="../assets/img/team-2.jpg" class="avatar avatar-sm  me-3 ">
                      </div>
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span class="font-weight-bold">New message</span>
                        </h6>
                        <p class="text-xs text-secondary mb-0">
                          <i class="fa fa-clock me-1"></i>
                          13 minutes ago
                        </p>
                      </div>
                    </div>
                  </a>
                </li>
                <li class="mb-2">
                  <a class="dropdown-item border-radius-md" href="javascript:;">
                    <div class="d-flex py-1">
                      <div class="my-auto">
                        <img src="../assets/img/small-logos/logo-spotify.svg" class="avatar avatar-sm bg-gradient-dark  me-3 ">
                      </div>
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span class="font-weight-bold">New album</span>by sudhakar mourya 
                        </h6>
                        <p class="text-xs text-secondary mb-0">
                          <i class="fa fa-clock me-1"></i>
                          1 day
                        </p>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>