@extends('layout.home')
@section('frontdata')
<!-- banner section start -->
<section class="bg-white bg-banner">
	<div class="container-fluid">
		<div class="row">
			<div class="slider">
				<img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
			</div>
		</div>
	</div>
</section>
<!-- banner section end -->
<!-- gallery section start -->
<section class="ti_padding_top_60 bg-white">
	<div class="container">
		<div class="row d-flex justify-content-center align-items-center">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ti_heading_wrapper text-center">
					<h2 class="title-heading fs-1 text-uppercase text-center pb-4">Crative Gallery</h2>
				</div>
			</div>
			<div class="col-sm-12 button-section pt-5 ms-0">
				<a href="{{ route('gallary') }}"><button type="button" id="btn-click1" class="btn-training active ps-5 pe-5">All Events</button></a>
				<a href="{{ route('gallary', 'company') }}"><button type="button" id="btn-click2" class="btn-training  ps-5 pe-5">Company</button></a>
				<a href="{{ route('gallary', 'collage') }}"><button type="button" id="btn-click3" class="btn-training   ps-5 pe-5">College</button></a>
				<a href="{{ route('gallary', 'student') }}"><button type="button" id="btn-click4" class="btn-training   ps-5 pe-5">Student</button></a>
				<a href="{{ route('gallary', 'seminor') }}"><button type="button" id="btn-click5" class="btn-training   ps-5 pe-5">Seminor</button></a>
			</div>
		</div>
	</div>
	<div class="container" id="seminor">
		<div class="row">
			@forelse($galleries as $gallery)
			<div class="col-lg-3 col-md-6 col-sm-12 pt-4">
				<div class="card">
					<div class="gallery-image">
						<img src="{{ env('FILE_URL').$gallery->image }}" class="pop img-fluid">
					</div>
					<b>{{ $gallery->description }}</b>
				</div>
			</div>
			@empty
			<p style="color: red; text-align: center"> <b>Record not found</b> </p>
			@endforelse
		</div>
	</div>

</section>

<!-- Modal -->
<div class="modal" id="imagemodal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body m-auto">
				<img src="" id="imagepreview" class="img-fluid">
			</div>
		</div>
	</div>
</div>

<!-- gallery section end  -->
@endsection