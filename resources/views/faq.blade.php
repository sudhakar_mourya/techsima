@extends('layout.home')
@section('frontdata')
<!-- banner section start -->
<section class="bg-white bg-banner">
	<div class="container-fluid">
		<div class="row">
			<div class="slider">
				<img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
			</div>
		</div>
	</div>
</section>
<!-- banner section end -->
<section class="ti_padding_top_60 bg-white faq-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 faq-title">
				<h2 class="title-heading fs-1 text-uppercase text-center pb-4">FAQ</h2>
				<h3 class="fs-3 text-uppercase pb-4">Frequently Asked Questions ?</h3>
			</div>
			
			<div class="accordion accordion-flush" id="accordionFlushExample">
				@foreach($faqs as $faq)
					<div class="accordion-item">
						<h2 class="accordion-header" id="flush-headingOne{{$faq->id}}">
							<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne{{$faq->id}}" aria-expanded="false" aria-controls="flush-collapseOne">
								{{$faq->question}}
							</button>
						</h2>
						<div id="flush-collapseOne{{$faq->id}}" class="accordion-collapse collapse" aria-labelledby="flush-headingOne{{$faq->id}}" data-bs-parent="#accordionFlushExample">
							<div class="accordion-body">{{$faq->answer}}</div>
						</div>
					</div>
				@endforeach
			</div>
			<!-- <div class="accordion" id="accordion">
				<div class="accordion-item">
					<h2 class="accordion-header" id="headingTwo">
						<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="{{'#collapse'. $faq->id}}" aria-expanded="false" aria-controls="{{'collapse'. $faq->id}}">
							{{$faq->question}}
						</button>
					</h2>
					<div id="{{'collapse'. $faq->id}}" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordion">
						<div class="accordion-body">
							{{$faq->answer}}
						</div>
					</div>
				</div>
			</div> -->
		</div>
	</div>
</section>

<!-- Faq Review Section Start -->
<section class="ti_padding_top_60 bg-white">
	<div class="container">
		<div class="row d-flex justify-content-center align-items-center">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ti_heading_wrapper text-center">
					<h2><span> <img src="{{asset('/assets/img/heading_icon.png') }}"></span> Student Ask Questions ?</h2>
					<p class="p-3 w-75 m-auto text-center pb-5">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Exercitationem eveniet, error et ipsa sint, minus quo nemo vitae maiores aliquam officiis, eos alias ipsum! Suscipit est tenetur reprehenderit rerum natus?</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="enquery-image">
					<img src="{{asset('assets/img/faq.jpg') }}">
				</div>
			</div>
			<div class="col-md-6 enquery-form">
				<form>
					<div class="mb-3">
						<label for="Input1" class="form-label"></label>
						<input type="text" class="form-control" id="Input1" placeholder="Full Name">
					</div>
					<div class="mb-3">
						<label for="Input1" class="form-label"></label>
						<input type="text" class="form-control" id="Input1" placeholder="Phone Number">
					</div>
					<div class="mb-3">
						<label for="Input1" class="form-label"></label>
						<input type="email" class="form-control" id="Input1" placeholder="Email address">
					</div>
					<div class="mb-3">
						<label for="Textarea1" class="form-label"></label>
						<textarea placeholder="Ask Question ?" class="form-control" id="Textarea1" rows="3"></textarea>
					</div>
					<button type="button" class="btn d-inline"><span>Submit Question</span></button>
				</form>
			</div>
		</div>
	</div>
</section>
<!-- Faq Review Section End -->
@endsection