@extends('layout.home')
@section('frontdata')
<!-- banner section start -->
<section class="bg-white bg-banner">
	<div class="container-fluid">
		<div class="row">		
			<div class="slider">
          <img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
			</div>
		</div>
	</div>
</section>
<!-- banner section end -->
<section class="ti_padding_top_60">
  <div class="container">
    <div class="row">
      <div class="col-lg-3"></div>
      <div class="col-lg-6 login p-5">
        @if(Session::has('success'))
        <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
        @endif
        <h2 class="widget-title text-center">SignIn</h2>
        <form method="post" action="{{route('signin')}}">
          @csrf
          <div class="mb-3">
            <label for="InputEmail1" class="form-label">Full Name</label>
            <input type="text" class="form-control" name="name" id="InputEmail1" onclick="hideError(this)" aria-describedby="emailHelp">
            @if($errors->has('name'))
            <div class="text-danger" id="name">{{ $errors->first('name') }}</div>
            @endif
          </div>
          <div class="mb-3">
            <label for="InputEmail1" class="form-label">Email</label>
            <input type="text" class="form-control" name="email" id="InputEmail1" onclick="hideError(this)" aria-describedby="emailHelp">
            @if($errors->has('email'))
            <div class="text-danger" id="email">{{ $errors->first('email') }}</div>
            @endif
          </div>
          <label for="password1" class="form-label">Password</label>
          <div class="input-group mb-3">            
            <input type="password" class="form-control" name="password" id="password1" onclick="hideError(this)">
            <span class="input-group-text"><i class="far fa-eye-slash eye1" id="togglePassword"></i></span>
            @if($errors->has('password'))
            <div class="text-danger" id="password">{{ $errors->first('password') }}</div>
            @endif
          </div>
          <label for="password2" class="form-label">Conform Password</label>
          <div class="input-group mb-3">           
            <input type="password" class="form-control" name="confirm_password" id="password2" onclick="hideError(this)"/>
           <span class="input-group-text"><i class="far fa-eye-slash eye2" id="togglePassword"></i></span>
            @if($errors->has('confirm_password'))
            <div class="text-danger" id="confirm_password">{{ $errors->first('confirm_password') }}</div>
            @endif
          </div>
          <button type="submit" class="btn-submit">Sign In</button>
        </form>
        <span>Already have an account ?</span><span><a href="#">Sign In</a></span>
      </div>
      <div class="col-lg-3"></div>
    </div>
  </div>
</section>
@endsection