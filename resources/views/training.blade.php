@extends('layout.home')
@section('frontdata')
<!-- banner section start -->
<section class="bg-white">
	<div class="container-fluid">
		<div class="row">
			<!-- <div class="col-lg-6 background-slider">
				<div class="row background-slider-outer">				
                    <div class="breadcrumb-section d-flex justify-content-center align-items-center">
                        <nav aria-label="breadcrumb ">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Library</li>
                            </ol>
                        </nav>
                    </div>
				</div>
			</div> -->
			<div class="slider">
                <img src="{{ isset($baner->poster) ? env('FILE_URL').$baner->poster : '' }}">
			</div>
		</div>
	</div>
</section>
<!-- banner section end -->
<!-- Our training section start -->
<section class="ti_padding_top_60 training-main-section">
	<div class="container px-3">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="ti_heading_wrapper text-center">
					<h2><span> <img src="{{asset('/assets/img/heading_icon.png') }}"></span>Our Training</h2>
					<p class="p-3 w-75 m-auto text-center pb-5">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Exercitationem eveniet, error et ipsa sint, minus quo nemo vitae maiores aliquam officiis, eos alias ipsum! Suscipit est tenetur reprehenderit rerum natus?</p>
				</div>
			</div>
			@foreach($training as $item)
			<div class="col-lg-4 col-md-6 col-sm-12 training-section">
				<div class="training-outer">
					<div class="training-inner">
						<h5 class="card-title">{{$item->name}}</h5>
						<p class="card-text">{{$item->short_description}}</p>
						<a href="#" class="btn-training">View More </a>
					</div>
				</div>
			</div>
			@endforeach			
		</div>
	</div>
</section>
<!-- Our training section End -->
@endsection