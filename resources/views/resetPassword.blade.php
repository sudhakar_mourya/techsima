@extends('layout.home')
@section('frontdata')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 forget-pwd-section">
               <h3 class="text-center pb-4 text-uppercase">Reset Password</h3> 
                @if(Session::has('success'))
                    <h6 class="text-success" id="success">{{ Session::get('success') }}</h6>
                @endif
                <form action="{{route('password.reset')}}" method="post">
                        @csrf
                    <div class="mb-3">
                        <label for="password" class="form-label">Enter New Password</label>
                        <input type="password" class="form-control" name="password" id="password" aria-describedby="emailHelp">
                        @if($errors->has('password'))
                            <div class="text-danger" id="password">{{ $errors->first('password') }}</div>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="confirm_password" class="form-label">Enter Confirm Password</label>
                        <input type="confirm_password" class="form-control" name="confirm_password" id="confirm_password" aria-describedby="emailHelp">
                        @if($errors->has('confirm_password'))
                            <div class="text-danger" id="confirm_password">{{ $errors->first('confirm_password') }}</div>
                        @endif
                    </div>
                    <button type="submit" class="btn-training">Reset Password</button>
                </form>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
@endsection