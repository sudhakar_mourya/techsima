@extends('layout.home')
@section('frontdata')
<section class="error-page">
    <div class="container-fluid">
        <div class="row">
            <div class="error-page-inner">
                <h1>404</h1>
                <h2>Page Not found</h2>
                <a class="btn-error" href="{{ route('home') }}">GO BACK TO HOME</a>
            </div>            
        </div>
    </div>
</section>
@endsection