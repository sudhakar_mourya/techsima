@extends('layout.home')
@section('frontdata')
<section class="service-details-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="service-details-banner-inner">
                <h1 class="main-heading">Software Deplovement</h1>
            </div>
        </div>
    </div>
</section>
<section class="services-details">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 service-details-page">
                <h2 class="heading">{{ $service->name }}</h2>
                <div class="service-poster-image">
                    <img src="{{ env('FILE_URL').$service->image }}" class="img-fluid"/>
                        <h3 class="sub-heading">{{ $service->name }}</h3>
                            <p class="content">
                                {{ $service->description }}
                            </p>
                </div>
            </div>
            <div class="col-lg-3">
                <h2 class="pb-3">Services</h2>
                <ul class="service-list">
                    <li>
                        <img src="{{asset('assets/img/check-circle.svg')}}" class="img-fluid"/>
                        <span>Web Devlopment</span>
                    </li>
                    <li>
                        <img src="{{asset('assets/img/check-circle.svg')}}" class="img-fluid"/>
                        <span>Web Devlopment</span>
                    </li>
                    <li>
                        <img src="{{asset('assets/img/check-circle.svg')}}" class="img-fluid"/>
                        <span>Web Devlopment</span>
                    </li>
                    <li>
                        <img src="{{asset('assets/img/check-circle.svg')}}" class="img-fluid"/>
                        <span>Web Devlopment</span>
                    </li>
                    <li>
                        <img src="{{asset('assets/img/check-circle.svg')}}" class="img-fluid"/>
                        <span>Web Devlopment</span>
                    </li>
                    <li>
                        <img src="{{asset('assets/img/check-circle.svg')}}" class="img-fluid"/>
                        <span>Web Devlopment</span>
                    </li>
                    <li>
                        <img src="{{asset('assets/img/check-circle.svg')}}" class="img-fluid"/>
                        <span>Web Devlopment</span>
                    </li>
                </ul>
            </div>
            <div class="service-plan">
                <h2>Planing and Analysis list</h2>
                <ul class="service-list">
                    @foreach(explode(',', $service->title) as $item)
                    <li><i class="far fa-check-circle"></i><span>{{ $item }}</span></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>    
</section>
<section class="services-details">
    <div class="container">
        <div class="row">
            <h2>Which type of software provide our company?</h2>
            @foreach($servicedetail as $item)
            <div class="col-md-6 project-make">
                <div class="row">
                    <div class="col-md-3">
                        <div class="service-image-logo">
                            <img src="{{ env('FILE_URL').$item->image }}" alt="service-img" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <h3>{{ $item->name }}</h3>
                        <p>{{ $item->description }}</p>
                    </div>
                </div>                
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection