<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Password;
use App\Models\User;
use App\Models\Poster;
use App\Models\Contact;
use App\Models\Subscribe;
use App\Models\Blog;
use App\Models\Faq;
use App\Models\Testimonial;
use App\Models\Course;
use App\Models\Enquiry;
use App\Models\Gallery;
use App\Models\Register;
use App\Models\Training;
use App\Models\Service;
use App\Models\ServiceDetail;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{
    public function signIn(Request $request){
        try{
            if($request->isMethod('post')){
                $rule = [
                    'name'=>'required|min:3|max:25',
                    'email'=>'required|email|unique:users',
                    'password'=>'required|min:8|max:16',
                    'confirm_password'=>'required|min:8|max:16|same:password',
                    'g-recaptcha-response' => 'required|captcha',
                ];
                if($validate = validateRequest($request, $rule)){
                    return returnWithError($validate);
                }else{
                    $user = new User;
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->password = Hash::make($request->password);
                    $user->save();
                    return returnSuccess('Your signin successfully.Please login to your email and password');
                }
            }else{
                $data = Poster::select('poster')->where('page_name', 'signin')->orderBy('created_at', 'DESC')->first();
                return view('signin', ['baner' => $data]);
            }
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function login(Request $request){
        try{
            if($request->isMethod('post')){
                $rule = [
                    'email'=>'required|email',
                    'password'=>'required|min:8|max:16',
                ];
                if($validate = validateRequest($request, $rule)){
                    return returnWithError($validate);
                }else{
                    $data = User::where('email', $request->email)->first();
                    if(empty($data)){
                        return returndanger('Your email or password is incorrect.');
                    }else{
                        if(!Hash::check($request->password, $data->password)){
                            return returndanger('Your email or password is incorrect.');
                        }else{
                            Auth::login($data);
                            if($data->role == 'admin')
                                return returnSuccess('', '/admin');
                            else
                            return returnSuccess('', '/dashboard');
                        }
                    }               
                }
            }else{
                if(Auth::user()){
                    if (Auth::user()->role == 'admin')
                        return returnSuccess('', '/admin');
                    else
                        return returnSuccess('', '/dashboard');
                }
                $data = Poster::select('poster')->where('page_name', 'login')->orderBy('created_at', 'DESC')->first();
                return view('login', ['baner' => $data]);
            }
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function logout(Request $request) {
        try{
            Auth::logout();
            return redirect('/login');
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function register(Request $request){
        try{
            if($request->isMethod('post')){
                $rule = [
                    'training_mode' => 'required',
                    'technology' => 'required',
                    'training_name' => 'required',
                    'education' => 'required',
                    'course_year' => 'required',
                    'name'=>'required|min:3|max:25',
                    'father_name'=>'required|min:3|max:25',
                    'email'=>'required|email|unique:users',
                    'mobile'=>'required|min:10|max:15',
                    'college_name'=>'required',
                    'payment_type'=>'required',
                    'payment_mode'=>'required',
                    'g-recaptcha-response' => 'required|captcha',
                ];
                if($validate = validateRequest($request, $rule)){
                    return returnWithError($validate);
                }else{
                    $register = new Register;
                    saveData($register, $request);

                    $user = new User;
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->password = Hash::make($request->mobile);
                    $user->save();
                    return returnSuccess('Your registation successfully');
                }
            }else{
                return view('register');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function dashboard(Request $request) {
        try{
            return view('admin.pages.dashboard');
        }catch(\Exception $e){
            return view('status');
        }       
    }


    // for only admin/folder/file start

    public function delete($model, $id) {
        try{
            if($model == 'poster')
                $modal_name = Poster::find($id);
            if($model == 'contact')
                $modal_name = Contact::find($id);
            if($model == 'user')
                $modal_name = User::find($id);
            if($model == 'subscribe')
                $modal_name = Subscribe::find($id);
            if($model == 'faq')
                $modal_name = Faq::find($id);
            if($model == 'testimonial')
            $modal_name = Testimonial::find($id);
            if($model == 'blog')
                $modal_name = Blog::find($id);
            if($model == 'course')
                $modal_name = Course::find($id);
            if($model == 'enquiry')
                $modal_name = Enquiry::find($id);
            if($model == 'gallery')
                $modal_name = Gallery::find($id);
            if($model == 'training')
                $modal_name = Training::find($id);
            if($model == 'register')
                $modal_name = Register::find($id);
            if($model == 'service')
                $modal_name = Service::find($id);

            $modal_name->delete();
            return returnSuccess(ucfirst($model). ' deleted Successfully');
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function edit($model, $id) {
        try{
            if($model == 'poster')
                $data = Poster::find($id);
            if($model == 'faq')
                $data = Faq::find($id);
            if($model == 'testimonial')
                $data = Testimonial::find($id);
            if($model == 'blog')
                $data = Blog::find($id);
            if($model == 'course')
                $data = Course::find($id);
            if($model == 'gallery')
                $data = Gallery::find($id);
            if($model == 'training')
                $data = Training::find($id);
            if($model == 'register')
                $data = Register::find($id);
            if($model == 'service')
                $data = Service::find($id);

            return view('admin.'. $model. '.edit'. ucFirst($model), ['record' => $data]);
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function add($model) {
        try{
            return view('admin.'. $model. '.add'. ucFirst($model));
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function view($model){
        try{
            if($model == 'poster')
                $data = Poster::get();
            if($model == 'contact')
                $data = Contact::get();
            if($model == 'subscribe')
                $data = Subscribe::get();
            if($model == 'faq')
                $data = Faq::get();
            if($model == 'testimonial')
                $data = Testimonial::get();
            if($model == 'blog')
                $data = Blog::get();
            if($model == 'course')
                $data = Course::get();
            if($model == 'enquiry')
                $data = Enquiry::get();
            if($model == 'gallery')
                $data = Gallery::get();
            if($model == 'training')
                $data = Training::get();
            if($model == 'register')
                $data = Register::get();
            if($model == 'service')
                $data = Service::get();
            if($model == 'user')
                $data = User::select('user_details.*', 'users.id', 'users.name', 'users.email')
                        ->leftJoin('user_details', 'users.id', 'user_details.user_id')->get();

            return view('admin.'. $model. '.'. $model, ['records' => $data]);
        }catch(\Exception $e){
            return view('status');
        }
    }


    public function forgetPassword(Request $request){
        try{
            if($request->isMethod('post')){
                $rule = [
                    'email' => 'required|email|max:150|exists:users',
                ];
                
                if($validate = validateRequest($request, $rule)){
                    return returnWithError($validate);
                }else{
                    $response = Password::broker()->sendResetLink($request->only('email'));
                    return returnSuccess($response);
                }
            }else{
                return view('forgetPassword');
            }
        } catch (Exception $e) {
            return view('status');
        }
    }

    public function resetPassword(Request $request){
        try{
            if($request->isMethod('post')){
                $rule = [
                    'email' => 'required|email|max:150',
                    'password' => 'required|min:8|max:16',
                    'confirm_password' => 'required|min:8|max:16|same:password',
                    'token' => 'required',
                ];
        
                if($validate = validateRequest($request, $rule)){
                    return returnWithError($validate);
                }else{
                        $status = Password::broker()->reset(
                            $this->credentials($request), function ($user, $password) {
                                $user->password = Hash::make($password);
                                $user->save();

                            }
                        );
                        $message = $status === Password::PASSWORD_RESET
                                    ? $status
                                    : ValidationException::withMessages([
                                        'email' => [trans($status)],
                                    ]);
                        return returnSuccess($message);
                }
            }else{
                return view('resetPassword');
            }
        } catch (Exception $e) {
            return view('status');
        }
    }

    protected function credentials(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
    }

    // for only admin/folder/file end

    public function changepassword(Request $request){
        try{
            if($request->isMethod('post')){
                $rule = [
                    'current_password' => 'required|min:8|max:16',
                    'new_password' => 'required|min:8|max:16',
                    'confirm_password' => 'required|min:8|max:16|same:new_password',
                ];
        
                if($validate = validateRequest($request, $rule)){
                    return returnWithError($validate);
                }
                $id = Auth::id();
                $dbpass = User::find($id)->password;
                if(Hash::check($request->current_password, $dbpass)){
                    if ($request->new_password != $request->current_password) {
                        if ($request->new_password == $request->confirm_password) {
                            $pass = Hash::make($request->new_password);
                            dd($pass);
                        }else{
                            session()->put('error', 'your confirm password must be same to new password');
                            return redirect()->back();
                        }
                    }else{

                        session()->put('error', 'your new password can not be old password');
                        return redirect()->back();
                    }
                }else{
                session()->put('error', 'your current password is wrong');
                    return redirect()->back();
                }
            }else{
                return view('changePassword');
            }
        } catch (Exception $e) {
            return view('status');
        }
        
    }


}
