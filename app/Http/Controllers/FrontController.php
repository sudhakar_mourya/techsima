<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Contact;
use App\Models\Poster;
use App\Models\Subscribe;
use App\Models\Enquiry;
use App\Models\Gallery;
use App\Models\Course;
use App\Models\Faq;
use App\Models\Training;
use App\Models\Service;
use App\Models\ServiceDetail;

class FrontController extends Controller
{
    public function home(){
        try{
            $data = Poster::select('poster')->where('page_name', 'home')->orderBy('created_at', 'DESC')->first();
            $course = Course::select('name', 'logo', 'title')->limit(3)->get();
            $training = Training::select('id', 'name', 'image', 'title', 'short_description')->limit(6)->get();
            $service = Service::select('id', 'name', 'image', 'title', 'short_description')->limit(6)->get();
            $record = [
                'baner' => $data,
                'course' => $course,
                'training' => $training,
                'service' => $service,
            ];
            return view('index', $record);
        }catch(\Exception $e){
            return view('status');
        }
    }
    public function aboutus(){
        try{
            $data = Poster::select('poster')->where('page_name', 'about-us')->orderBy('created_at', 'DESC')->first();
            return view('aboutus', ['baner' => $data]);
        }catch(\Exception $e){
            return view('status');
        }
    }
    public function traning(){
        try{
            $data = Poster::select('poster')->where('page_name', 'training')->orderBy('created_at', 'DESC')->first();
            $training = Training::select('id', 'name', 'title', 'short_description')->get();
            return view('training', ['baner' => $data, 'training' => $training]);
        }catch(\Exception $e){
            return view('status');
        }
    }
    public function faq(){
        try{
            $data = Poster::select('poster')->where('page_name', 'faq')->orderBy('created_at', 'DESC')->first();
            $faq = Faq::take(10)->orderBy('created_at', 'DESC')->get();
            return view('faq', ['baner' => $data, 'faqs' => $faq]);
        }catch(\Exception $e){
            return view('status');
        }
    }
    public function courses(){
        try{
            $data = Poster::select('poster')->where('page_name', 'courses')->orderBy('created_at', 'DESC')->first();
            $frontend = Course::select('name', 'logo')->where('category', 'frontend')->get();
            $backend = Course::select('name', 'logo')->where('category', 'backend')->get();
            $mobile = Course::select('name', 'logo')->where('category', 'mobile')->get();
            $record = [
                'baner' => $data,
                'frontend' => $frontend,
                'backend' => $backend,
                'mobile' => $mobile,
            ];
            return view('courses', $record);
        }catch(\Exception $e){
            return view('status');
        }
    }
    public function gallery(Request $request){
        try{
            $category = $request->category ? $request->category : 'all';
            $data = Poster::select('poster')->where('page_name', 'gallery')->orderBy('created_at', 'DESC')->first();
            if($request->category){
                $gallery = Gallery::where('categories', $category)->get();
            }else{
                $gallery = Gallery::get();
            }
            return view('gallery', ['baner' => $data, 'galleries' => $gallery, 'tab' => $category]);
        }catch(\Exception $e){
            return view('status');
        }
    }
    public function services(){
        try{
            $data = Poster::select('poster')->where('page_name', 'services')->orderBy('created_at', 'DESC')->first();
            $service = Service::select('id', 'name', 'title', 'image', 'short_description')->get();
            return view('services', ['baner' => $data, 'service' => $service]);
        }catch(\Exception $e){
            return view('status');
        }
    }
    public function serviceDetail($id){
        $service = Service::select('id', 'name', 'title', 'image', 'description')->find($id);
        $servicedetail = ServiceDetail::select('id', 'name', 'title', 'image', 'description')->where('category', 'service')->where('foreign_id', $id)->get();
        return view('service-details', ['service' => $service, 'servicedetail' => $servicedetail]);
    }
    public function registration(){
        try{
            $data = Poster::select('poster')->where('page_name', 'registration')->orderBy('created_at', 'DESC')->first();
            return view('registration', ['baner' => $data]);
            return view('registration');
        }catch(\Exception $e){
            return view('status');
        }
    }
    public function contact(Request $request){
        try{
            if($request->isMethod('post')){
                $rule = [
                    'name'=>'required',
                    'last_name'=>'required',
                    'email'=>'required|email',
                    'phone'=>'required|min:10|max:15',
                    'message'=>'required'
                ];
                if($validate = validateRequest($request, $rule)){
                    return returnWithError($validate);
                }else{
                    $contact = new Contact;
                    saveData($contact, $request);
                    return returnSuccess('added successfully');
                }
            }else{
                $data = Poster::select('poster')->where('page_name', 'contact-us')->orderBy('created_at', 'DESC')->first();
                return view('contactus', ['baner' => $data]);
            }
        }catch(\Exception $e){
            return view('status');
        }
    }
    public function subscribe(Request $request){
        try{
            $rule = [
                'email'=>'required|email|unique:subscribes',
            ];
            $custom_message = [
                'email.unique' => 'You are already subscribe',
            ];
            if($validate = validateRequest($request, $rule, $custom_message)){
                return returnWithError($validate);
            }else{
                $subscribe = new Subscribe;
                saveData($subscribe, $request);
                return returnSuccess('added successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }
    public function enquiry(Request $request){
        try{
            $rule = [
                'name'=>'required',
                'email'=>'required|email',
                'phone'=>'required|min:10|max:15',
                'enquiry'=>'required'
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $enquiry = new Enquiry;
                saveData($enquiry, $request);
                return returnSuccess('added successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }

}
