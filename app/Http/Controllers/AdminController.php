<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Poster;
use App\Models\Blog;
use App\Models\Faq;
use App\Models\Course;
use App\Models\Gallery;
use App\Models\Testimonial;
use App\Models\Training;
use App\Models\Service;
use App\Models\ServiceDetail;

class AdminController extends Controller
{
    
    public function addPoster(Request $request){
        try{
            $rule = [
                'name'=>'required',
                'page_name'=>'required',
                'poster'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $poster_image = uploadFiles($request, 'poster', 'poster');
                $poster = new Poster;
                $poster->poster = $poster_image;
                saveData($poster, $request, ['poster']);
                return returnSuccess('Poster added successfully');
            }
        }catch(\Exception $e){
           return view('status'); 
        }
    }

    public function editPoster(Request $request, $id){
        try{
            $rule = [
                'name'=>'required',
                'page_name'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $poster_image = uploadFiles($request, 'poster', 'poster');
                $poster = Poster::find($id);
                $poster->poster = $poster_image ? $poster_image : $poster->poster;
                saveData($poster, $request, ['poster']);
                return returnSuccess('Poster updated successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function addBlog(Request $request){
        try{
            $rule = [
                'heading'=>'required',
                'title'=>'required',
                'sub_title'=>'required',
                'image'=>'required',
                'description'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $blog_image = uploadFiles($request, 'image', 'blog');
                $blog = new Blog;
                $blog->image = $blog_image;
                saveData($blog, $request, ['image']);
                return returnSuccess('Blog added successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function editBlog(Request $request, $id){
        try{
            $rule = [
                'heading'=>'required',
                'title'=>'required',
                'sub_title'=>'required',
                'description'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $blog_image = uploadFiles($request, 'image', 'blog');
                $blog = Blog::find($id);
                $blog->image = $blog_image ? $blog_image : $blog->image;
                saveData($blog, $request, ['image']);
                return returnSuccess('Blog updated successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function addFaq(Request $request){
        try{
            $rule = [
                'question'=>'required',
                'answer'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $faq = new Faq;
                saveData($faq, $request);
                return returnSuccess('Blog added successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }
    
    public function editFaq(Request $request, $id){
        try{
            $rule = [
                'question'=>'required',
                'answer'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $faq = Faq::find($id);
                saveData($faq, $request);
                return returnSuccess('Blog updated successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }
    
    public function addTestimonial(Request $request){
        try{
            $rule = [
                'name'=>'required',
                'desination'=>'required',
                'review'=>'required',
                'image'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $testimonial_image = uploadFiles($request, 'image', 'testimonial');
                $testimonial = new Testimonial;
                $testimonial->image = $testimonial_image;
                saveData($testimonial, $request, ['image']);
                return returnSuccess('Testimonial added successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function editTestimonial(Request $request, $id){
        try{
            $rule = [
                'name'=>'required',
                'desination'=>'required',
                'review'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $testimonial_image = uploadFiles($request, 'image', 'testimonial');
                $testimonial = Testimonial::find($id);
                $testimonial->image = $testimonial_image ? $testimonial_image : $testimonial->image;
                saveData($testimonial, $request, ['image']);
                return returnSuccess('Testimonial updated successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }
        
    public function addCourse(Request $request){
        try{
            $rule = [
                'name'=>'required',
                'title'=>'required',
                'sub_title'=>'required',
                'author_name'=>'required',
                'category'=>'required',
                'logo'=>'required',
                'image'=>'required',
                'date'=>'required',
                'description'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $course_image = uploadFiles($request, 'image', 'course');
                $logo = uploadFiles($request, 'logo', 'logo');
                $course = new Course;
                $course->image = $course_image;
                $course->logo = $logo;
                saveData($course, $request, ['image', 'logo']);
                return returnSuccess('Course added successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }
        
    public function editCourse(Request $request, $id){
        try{
            $rule = [
                'name'=>'required',
                'title'=>'required',
                'sub_title'=>'required',
                'author_name'=>'required',
                'category'=>'required',
                'date'=>'required',
                'description'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $course_image = uploadFiles($request, 'image', 'course');
                $logo = uploadFiles($request, 'logo', 'logo');
                $course = Course::find($id);
                $course->image = $course_image ? $course_image : $course->image;
                $course->logo = $logo ? $logo : $course->logo;
                saveData($course, $request, ['image', 'logo']);
                return returnSuccess('Course updated successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }
        
    public function addGallery(Request $request){
        try{
            $rule = [
                'description'=>'required',
                'image'=>'required',
                'categories'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $gallery_image = uploadFiles($request, 'image', 'gallery');
                $gallery = new Gallery;
                $gallery->image = $gallery_image;
                saveData($gallery, $request, ['image']);
                return returnSuccess('Gallery added successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function editGallery(Request $request, $id){
        try{
            $rule = [
                'description'=>'required',
                'categories'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $gallery_image = uploadFiles($request, 'image', 'gallery');
                $gallery = Gallery::find($id);
                $gallery->image = $gallery_image ? $gallery_image : $gallery->image;
                saveData($gallery, $request, ['image']);
                return returnSuccess('Galley updated successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function addTraining(Request $request){
        try{
            $rule = [
                'name'=>'required',
                'title'=>'required',
                'image'=>'required',
                'short_description'=>'required',
                'description'=>'required',
                'long_description'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $training_image = uploadFiles($request, 'image', 'training');
                $training = new Training;
                $training->image = $training_image;
                saveData($training, $request, ['image']);
                return returnSuccess('Training added successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }
    
    public function editTraining(Request $request, $id){
        try{
            $rule = [
                'name'=>'required',
                'title'=>'required',
                'short_description'=>'required',
                'description'=>'required',
                'long_description'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $training_image = uploadFiles($request, 'image', 'training');
                $training = Training::find($id);
                $training->image = $training_image ? $training_image : $training->image;
                saveData($training, $request, ['image']);
                return returnSuccess('Training updated successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }
    
    public function addService(Request $request){
        try{
            $rule = [
                'name'=>'required',
                'title'=>'required',
                'image'=>'required',
                'short_description'=>'required',
                'description'=>'required',
                'long_description'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $service_image = uploadFiles($request, 'image', 'service');
                $service = new Service;
                $service->image = $service_image;
                saveData($service, $request, ['image']);
                return returnSuccess('Service added successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }
    
    public function editService(Request $request, $id){
        try{
            $rule = [
                'name'=>'required',
                'title'=>'required',
                'short_description'=>'required',
                'description'=>'required',
                'long_description'=>'required',
            ];
            if($validate = validateRequest($request, $rule)){
                return returnWithError($validate);
            }else{
                $service_image = uploadFiles($request, 'image', 'service');
                $service = Service::find($id);
                $service->image = $service_image ? $service_image : $service->image;
                saveData($service, $request, ['image']);
                return returnSuccess('Service updated successfully');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }
    public function addServiceDetail(Request $request){
        $rule = [
            'name'=>'required',
            'title'=>'required',
            'image'=>'required',
            'description'=>'required',
        ];
        if($validate = validateRequest($request, $rule)){
            return returnWithError($validate);
        }else{
            $service_image = uploadFiles($request, 'image', 'servicedetail');
            $servicedetail = new ServiceDetail;
            $servicedetail->image = $service_image;
            saveData($servicedetail, $request, ['image']);
            return returnSuccess('Service Detail added successfully');
        }
    }

    public function editServiceDetail(Request $request, $id){
        $rule = [
            'name'=>'required',
            'title'=>'required',
            'description'=>'required',
        ];
        if($validate = validateRequest($request, $rule)){
            return returnWithError($validate);
        }else{
            $service_image = uploadFiles($request, 'image', 'servicedetail');
            $servicedetail = ServiceDetail::find($id);;
            $servicedetail->image = $service_image ? $service_image : $servicedetail->image;
            saveData($servicedetail, $request, ['image']);
            return returnSuccess('Service Detail updated successfully');
        }
    }

}
