<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Answer;
use App\Models\USer;
use App\Models\StudentAnswer;

class QuestionController extends Controller
{
    public function addQuestion(Request $request){
        try{
            if($request->isMethod('post')){
                $rule = [
                    'topic'=>'required',
                    'question'=>'required',
                    'option1'=>'required',
                    'option2'=>'required',
                    'option3'=>'required',
                    'option4'=>'required',
                ];
                if($validate = validateRequest($request, $rule)){
                    return returnWithError($validate);
                }else{
                    $question = new Question;
                    $question->topic = $request->topic;
                    $question->question = $request->question;
                    $question->save();
                    for ($i=1; $i < 5; $i++) { 
                        $ans = "option$i";
                        $answer = new Answer;
                        $answer->question_id = $question->id;
                        $answer->is_correct = $i == 1 ? 1 : 0;
                        $answer->answer = $request->$ans;
                        $answer->save();
                    }
                    
                    return returnSuccess('Question uploaded successfully.');
                }
            }else{
                return view('admin.exam.addquestion');
            }
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function questionList(Request $request){
        try{
                $data = [];
                $question_id = Question::select('question', 'id', 'topic')->get();
                foreach ($question_id as $key => $value) {
                    $question = Answer::where('question_id', $value->id)->select('answer', 'id', 'is_correct')->get();
                    $value->option = $question;
                }
                return view('admin.exam.questionlist', ['records' => $question_id]);
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function deleteQuestion(Request $request){
        try{
            $question = Question::find($request->id)->delete();
            $answer = Answer::where('question_id', $request->id)->delete();
            return returnSuccess('Question deleted Successfully');
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function editQuestion(Request $request){
        try{
            if($request->isMethod('post')){
                $rule = [
                    'topic'=>'required',
                    'question'=>'required',
                    'option1'=>'required',
                    'option2'=>'required',
                    'option3'=>'required',
                    'option4'=>'required',
                ];
                if($validate = validateRequest($request, $rule)){
                    return returnWithError($validate);
                }else{
                    $question = Question::find($request->id);
                    $question->topic = $request->topic;
                    $question->question = $request->question;
                    $question->save();
                    Answer::where('question_id', $request->id)->delete();
                    for ($i=1; $i < 5; $i++) { 
                        $ans = "option$i";
                        $answer = new Answer;
                        $answer->question_id = $request->id;
                        $answer->is_correct = $i == 1 ? 1 : 0;
                        $answer->answer = $request->$ans;
                        $answer->save();
                    }
                    
                    return returnSuccess('Question updated successfully.');
                }
            }else{
                $question = Question::select('question', 'id', 'topic')->find($request->id);
                $question->answer = Answer::where('question_id', $question->id)->select('answer', 'id', 'is_correct')->get();
                return view('admin.exam.editquestion', ['question' => $question]);
            }
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function examStart(Request $request, $topic = ''){
        try{
            if($request->isMethod('post')){
                foreach ($request->except('_token') as $key => $value) {
                    $ans = new StudentAnswer;
                    $ans->student_id = auth()->user()->id;
                    $ans->question_id = $key;
                    $ans->answer_id = $value;
                    $ans->topic = $topic;
                    $ans->save();
                }
                return returnSuccess('Exam Compleated successfully.');
            }else{
                $question = Question::select('question', 'id')->where('topic', $topic)->limit(10)->orderByDesc('id')->get();
                foreach ($question as $key => $value) {
                    $answer = Answer::where('question_id', $value->id)->select('answer', 'id')->get();
                    $value->option = $answer;
                }
                return view('student.examstart', ['records' => $question, 'topic' => $topic]);
            }
            
        }catch(\Exception $e){
            return view('status');
        }
    }

    public function answerList(Request $request){
        try{
            $topic = Question::groupBy('topic')->pluck('topic');
            $student = User::pluck('name', 'id');
            $data = StudentAnswer::select('student_answers.id as id', 'student_answers.topic as topic', 'student_answers.updated_at as date', 'users.name as student', 'questions.question as question', 'answers.answer as answer')
                                    ->leftJoin('users', 'student_answers.student_id', 'users.id')
                                    ->leftJoin('questions', 'student_answers.question_id', 'questions.id')
                                    ->leftJoin('answers', 'student_answers.answer_id', 'answers.id');
            $data->when($request->has('student') && $request->student != '', function ($q) use($request) {
                return $q->where('student_answers.student_id', $request->student);
            });
            $data->when($request->has('topic') && $request->topic != '', function ($q) use($request) {
                return $q->where('student_answers.topic', $request->topic);
            });
            $data = $data->get();
            return view('admin.exam.answerlist', ['records' => $data, 'topic' => $topic, 'student' => $student]);
        }catch(\Exception $e){
            return view('status');
        }
    }




}
