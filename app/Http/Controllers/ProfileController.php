<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\UserDetail;

class ProfileController extends Controller
{
    public function updateProfile(Request $request){
        try{
            if($request->isMethod('post')){
                $rule = [
                    'name'=>'required',
                    'father_name'=>'required',
                    'phone'=>'required|min:8|max:16',
                    'high_quallification'=>'required',
                    'address' => 'required',
                    'image' => 'image',
                ];
                if($validate = validateRequest($request, $rule)){
                    return returnWithError($validate);
                }else{
                    $user = User::find(auth()->user()->id);
                    $user->name = $request->name;
                    $user->save();
                    
                    $image = uploadFiles($request, 'user', 'image');
                    $user_details = UserDetail::where('user_id', auth()->user()->id)->first();
                    if($user_details){
                        $user_details->father_name = $request->father_name;
                        $user_details->phone = $request->phone;
                        $user_details->high_quallification = $request->high_quallification;
                        $user_details->address = $request->address;
                        $user_details->image = $image;
                        $user_details->save();
                    }else{
                        $user_details = new UserDetail;
                        $user_details->user_id = auth()->user()->id;
                        $user_details->father_name = $request->father_name;
                        $user_details->phone = $request->phone;
                        $user_details->high_quallification = $request->high_quallification;
                        $user_details->address = $request->address;
                        $user_details->current_address = $request->current_address ?? "";
                        $user_details->status = $request->status ?? "";
                        $user_details->image = $image;
                        $user_details->save();
                    }
                    
                    return returnSuccess('Profile updated successfully.');
                }
            }else{
                $data = User::select('name', 'email', 'phone', 'father_name', 'high_quallification', 'image', 'address')
                                ->leftJoin('user_details', 'user_details.user_id', 'users.id')->find(auth()->user()->id);

                return view('student.updateprofile', ['profile' => $data]);
            }
        }catch(\Exception $e){
            return view('status');
        }
    }
}
