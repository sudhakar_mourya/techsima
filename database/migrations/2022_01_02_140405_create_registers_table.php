<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->id();
            $table->string('training_mode');
            $table->string('technology');
            $table->string('training_name');
            $table->string('education');
            $table->string('course_year');
            $table->string('name');
            $table->string('father_name');
            $table->string('email')->unique();
            $table->string('mobile');
            $table->string('alt_mobile');
            $table->string('college_name');
            $table->string('payment_type');
            $table->string('payment_mode');
            $table->string('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}
