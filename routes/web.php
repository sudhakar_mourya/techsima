<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\QuestionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontController::class,'home'])->name('home');
Route::get('aboutus',[FrontController::class,'aboutus'])->name('aboutus');
Route::match(['get','post'], 'contactus',[FrontController::class,'contact'])->name('contactus');
Route::post('enquiry',[FrontController::class,'enquiry'])->name('enquiry');
Route::post('subscribe',[FrontController::class,'subscribe'])->name('subscribe');
Route::get('traning',[FrontController::class,'traning'])->name('traning');
Route::get('faq',[FrontController::class,'faq'])->name('faq');
Route::get('registration',[FrontController::class,'registration'])->name('registration');
Route::get('courses',[FrontController::class,'courses'])->name('courses');
Route::get('services',[FrontController::class,'services'])->name('services');
Route::get('service-details/{id}',[FrontController::class,'serviceDetail'])->name('servicedetail');
Route::get('gallery/{category?}',[FrontController::class,'gallery'])->name('gallary');

Route::match(['get','post'], 'signin',[UserController::class,'signIn'])->name('signin');
Route::match(['get','post'], 'register',[UserController::class,'register'])->name('register');
Route::match(['get','post'], 'login',[UserController::class,'login'])->name('login');
Route::match(['get','post'], 'change-password',[UserController::class,'changePassword'])->name('change-password');
Route::match(['get','post'], 'forget-password',[UserController::class,'forgetPassword'])->name('forget-password');
Route::match(['get','post'], 'reset-password',[UserController::class,'resetPassword'])->name('password.reset');
Route::post('logout',[UserController::class,'logout'])->name('logout');

Route::get('dashboard/', function () {
    return view('student.studentdashboard');
});
Route::get('update/', function () {
    return view('student.updateprofile');
});

Route::get('exam/', function () {
    return view('student.exam');
});

Route::get('examstart/',function(){
    return view('student.examstart');
});

Route::get('fee/', function () {
    return view('student.fee');
});

Route::get('addqst/',function(){
    return view('admin.exam.addquestion');
});
Route::get('privacy', function () {
    return view('privacy');
});

Route::get('thankyou', function () {
    return view('thankyou');
});
Route::get('site', function () {
    return view('sitemap');
});


Route::group(['middleware' => ['auth', 'admin']], function(){
    Route::prefix('admin')->group(function () {
        
        Route::post('delete/{model}/{id}/', [UserController::class,'delete'])->name('delete');
        Route::get('edit/{model}/{id}/', [UserController::class,'edit'])->name('edit');
        Route::get('add/{model}/', [UserController::class,'add'])->name('add');
        Route::get('{model}/', [UserController::class,'view'])->name('view');


        Route::get('/', [UserController::class,'dashboard']);
        Route::post('add/poster/',[AdminController::class,'addPoster'])->name('addPoster');
        Route::post('edit/poster/{id}/',[AdminController::class,'editPoster'])->name('editPoster');
        Route::post('add/blog/',[AdminController::class,'addBlog'])->name('addBlog');
        Route::post('edit/blog/{id}/',[AdminController::class,'editBlog'])->name('editBlog');
        Route::post('add/faq/',[AdminController::class,'addFaq'])->name('addFaq');
        Route::post('edit/faq/{id}/',[AdminController::class,'editFaq'])->name('editFaq');
        Route::post('add/testimonial/',[AdminController::class,'addTestimonial'])->name('addTestimonial');
        Route::post('edit/testimonial/{id}/',[AdminController::class,'editTestimonial'])->name('editTestimonial');
        Route::post('add/course/',[AdminController::class,'addCourse'])->name('addCourse');
        Route::post('edit/course/{id}/',[AdminController::class,'editCourse'])->name('editCourse');
        Route::post('add/gallery/',[AdminController::class,'addGallery'])->name('addGallery');
        Route::post('edit/gallery/{id}/',[AdminController::class,'editGallery'])->name('editGallery');
        Route::post('add/training/',[AdminController::class,'addTraining'])->name('addTraining');
        Route::post('edit/taining/{id}/',[AdminController::class,'editTraining'])->name('editTraining');
        Route::post('add/service/',[AdminController::class,'addService'])->name('addService');
        Route::post('edit/service/{id}/',[AdminController::class,'editService'])->name('editService');
        Route::post('add/service/detail',[AdminController::class,'addServiceDetail'])->name('addServiceDetail');
        Route::post('edit/service/detail/{id}/',[AdminController::class,'editServiceDetail'])->name('editServiceDetail');

    });

    Route::match(['get','post'], 'add/question/', [QuestionController::class,'addQuestion'])->name('addQuestion');
    Route::match(['get','post'], 'edit/question/', [QuestionController::class,'editQuestion'])->name('editQuestion');
    Route::get('question/list', [QuestionController::class,'questionList'])->name('questionList');
    Route::get('answer/list', [QuestionController::class,'answerList'])->name('answerList');
    Route::post('deleteQuestion', [QuestionController::class,'deleteQuestion'])->name('deleteQuestion');

});


Route::group(['middleware' => ['auth']], function(){
    Route::get('exam/', function () {
        return view('student.exam');
    });
    Route::match(['get','post'], 'my-profile',[ProfileController::class,'updateProfile'])->name('updateProfile');
    Route::match(['get','post'], 'exam/start/{topic?}',[QuestionController::class,'examStart'])->name('examStart');



});


// Auth::routes();

