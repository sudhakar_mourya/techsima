$(document).ready(function() {
    /* Menu stcky start */
    $(window).scroll(function() {
        if ($(window).scrollTop() > 0) {
            $('#studentHeader').addClass('sticky');
        }
        if ($(window).scrollTop() < 0) {
            $('#studentHeader').removeClass('sticky');
        }
    });
});