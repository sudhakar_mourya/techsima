
/* menu start */
const primaryNav=document.querySelector(".primary-navigation");
const navToggle = document.querySelector(".mobile-nav-toggle");
navToggle.addEventListener("click", ()=>{
    const visiblity = primaryNav.getAttribute("data-visible");
    if(visiblity==="false"){
        primaryNav.setAttribute("data-visible", true);
        navToggle.setAttribute("aria-expanded", true);
    }
    else if(visiblity==="true"){
        primaryNav.setAttribute("data-visible", false);
        navToggle.setAttribute("aria-expanded", false);
    }
});
/* menu end */
/* loder start */

var loader = document.getElementById("preloader");
window.addEventListener("load", function() {
        loader.style.display = "none";
    })
function myFunction(x) {
    x.classList.toggle("change");
}

/* loder End */
  
    
$(document).ready(function() {
    /* tooltip start*/
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    /* tooltip start*/
    /* Menu stcky start */
        $(window).scroll(function(){
        if($(window).scrollTop()>114){
            $('.primary-header').addClass('sticky');
        }
        if($(window).scrollTop()<115){
            $('.primary-header').removeClass('sticky');
        }
    });
    /* Menu stcky End */
   /* toggle button Start */
    $(".menu-toggle").click(function() {
        $(".menu-toggle").toggleClass('active')
        $(".mobile-menu,nav").toggleClass('active')
    });
    /* toggle button End */
    /* testimonial Start */
    $(".testimonial-carousel").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: true,
        prevArrow: $(".testimonial-carousel-controls .prev"),
        nextArrow: $(".testimonial-carousel-controls .next")
    });
    /* testimonial End */
    /* gallery page start */
    $("#btn-click1").click(function() {
        $(" #seminor, #college, #student, #company").show("up")
    })
    $("#btn-click2").click(function () {
        $("#all-event, #seminor, #college, #student").hide("up")
        $("#company").show("up")
    })
    $("#btn-click3").click(function () {
        $("#all-event, #company, #seminor, #student").hide("up")
        $("#college").show("up")
    })
    $("#btn-click4").click(function () {
        $("#all-event, #company, #college, #seminor").hide("up")
        $("#student").show("up")
    })
    $("#btn-click5").click(function () {
        $("#all-event, #company, #college, #student").hide("up")
        $("#seminor").show("up")
    })
});
$(".pop").click( function () {
    $('#imagepreview').attr('src', $(this).attr('src'));
    $('#imagemodal').modal('show');    
});
/* gallery page End */
/* registration page start*/
$("#payment_type").change( function () {
    if($(this).val() == "registration_fee"){
        $("#amount").val('500')
    }else{
        $("#amount").val('5000')
    }    
});
/* registration page End*/
